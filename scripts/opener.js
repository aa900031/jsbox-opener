const rules = require('./rules');

exports.match = (link) => {
  for (let index = 0; index < rules.length; index++) {
    const rule = rules[index]
    const regex = new RegExp(rule.regex)
    const matches = regex.exec(link)
    if (!matches) continue

    return {
      rule,
      matches,
    }
  }
}

exports.scheme = (app, url, matches) => new Promise((resolve, reject) => {
  let format = app.format;
  if (format) {
    for (let idx = 1; idx < matches.length; ++idx) {
      format = format.replace("$" + idx, matches[idx])
    }

    resolve(format);
    return;
  }

  let script = app.script
  if (script) {
    eval(script);
    process(url, match => resolve(match));
    return;
  }

  reject();
});

function jsonRequest(url, completionHandler) {
  $http.get({
    url: url,
    handler: function(resp) {
      var data = resp.data
      completionHandler(data);
    }
  })
}
