const apps = require('./apps');

module.exports = [
  {
    "title": "Open Tweet",
    "regex": "http(?:s)?://(?:mobile\\.|www\\.|m\\.)?twitter\\.com/(?:@?[a-zA-Z0-9_]{1,15}/status|i/web/status|statuses)/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Twitter,
        "format": "twitter://status?id=$1"
      },
      {
        app: apps.keys.Tweetbot,
        "format": "tweetbot:///status/$1"
      },
      {
        app: apps.keys.Twitterrific,
        "format": "twitterrific:///tweet?id=$1"
      }
    ]
  },
  {
    "title": "Add Tweet to Collection",
    "regex": "http(?:s)?://(?:mobile\\.|www\\.|m\\.)?twitter\\.com/(?:@?[a-zA-Z0-9_]{1,15}/status|i/web/status|statuses)/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Charm,
        "format": "x-charm://collect/$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s?)://(?:mobile\\.|www\\.|m\\.)?twitter\\.com/(?:@)?(?!search)([a-zA-Z0-9_]{1,15})(?:/with_replies|/following|/followers|/media|/favorites)?/?(\\?.*)?$",
    "apps": [
      {
        app: apps.keys.Twitter,
        "format": "twitter://user?screen_name=$1"
      },
      {
        app: apps.keys.Tweetbot,
        "format": "tweetbot:///user_profile/$1"
      },
      {
        app: apps.keys.Twitterrific,
        "format": "twitterrific:///profile?screen_name=$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s?)://(?:mobile\\.|www\\.|m\\.)?twitter\\.com/intent/user\\?.*screen_name=?(?:@)?([a-zA-Z0-9_]{1,15}).*$",
    "apps": [
      {
        app: apps.keys.Twitter,
        "format": "twitter://user?screen_name=$1"
      },
      {
        app: apps.keys.Tweetbot,
        "format": "tweetbot:///user_profile/$1"
      },
      {
        app: apps.keys.Twitterrific,
        "format": "twitterrific:///profile?screen_name=$1"
      }
    ]
  },
  {
    "title": "Open Hashtag",
    "regex": "http(?:s?)://(?:mobile\\.|www\\.|m\\.)?twitter\\.com/hashtag/([a-zA-Z0-9_]+).*$",
    "apps": [
      {
        app: apps.keys.Twitter,
        "format": "twitter://search?query=%23$1"
      },
      {
        app: apps.keys.Tweetbot,
        "format": "tweetbot:///search?query=%23$1"
      },
      {
        app: apps.keys.Twitterrific,
        "format": "twitterrific:///search?q=%23$1"
      }
    ]
  },
  {
    "title": "Open Search",
    "regex": "http(?:s)://(?:mobile\\.|www\\.|m\\.)?twitter\\.com/search.*q=([^&]+).*$",
    "apps": [
      {
        app: apps.keys.Twitter,
        "format": "twitter://search?query=$1"
      },
      {
        app: apps.keys.Tweetbot,
        "format": "tweetbot:///search?query=$1"
      },
      {
        app: apps.keys.Twitterrific,
        "format": "twitterrific:///search?q=$1"
      }
    ]
  },
  {
    "title": "Open Moment",
    "regex": "http(?:s?)://(?:mobile\\.|www\\.)?twitter\\.com/i/moments/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Twitter,
        "format": "twitter://moment?moment_id=$1"
      }
    ]
  },
  {
    "title": "Open Live Moment",
    "regex": "http(?:s?)://(?:mobile\\.|www\\.|m\\.)?twitter\\.com/i/live/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Twitter,
        "format": "twitter://live/timeline/$1"
      }
    ]
  },
  {
    "title": "Open Collection",
    "regex": "http(?:s)://(?:mobile\\.|www\\.|m\\.)?twitter\\.com/(?:@)?([a-zA-Z0-9_]{1,15})/timelines/(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Twitter,
        "format": "twitter://custom_timeline?screen_name=$1&timeline_id=$2"
      }
    ]
  },
  {
    "title": "Open Mute Filter",
    "regex": "http(?:s)?://(?:www\\.)?tapbots\\.com.*\\?url=(.*)$",
    "apps": [
      {
        app: apps.keys.Tweetbot,
        "script": "function process(url, completionHandler) { completionHandler('tweetbot:///' + decodeURIComponent(url.split('tweetbot%3A%2F%2F%2F')[1])) }"
      }
    ]
  },
  {
    "title": "Open Muffle",
    "regex": "http(?:s)?://(?:www\\.)?twitterrific\\.com/ios/muffle(?:/)?\\?add=([^&]*)$",
    "apps": [
      {
        app: apps.keys.Twitterrific,
        "format": "twitterrific://muffle?add=$1"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://(?:www\\.)?(?:instagram\\.com|instagr\\.am)/p/([a-zA-Z0-9_\\-\\.]+).*$",
    "apps": [
      {
        app: apps.keys.Instagram,
        "script": "function process(url, completionHandler) { jsonRequest('https://api.instagram.com/oembed?url=' + url, function(res) { if (res != null) { var mediaIdentifier = res['media_id']; } if (mediaIdentifier != null) { completionHandler('instagram://media?id=' + mediaIdentifier); } else { completionHandler(null); } } ); }"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:www\\.)?(?:instagram\\.com|instagr\\.am)/(?!explore)([a-zA-Z0-9_\\.]{1,30})(?:/|/?\\?.*)?$",
    "apps": [
      {
        app: apps.keys.Instagram,
        "format": "instagram://user?username=$1"
      }
    ]
  },
  {
    "title": "Open Tag",
    "regex": "http(?:s)?://(?:www\\.)?(?:instagram\\.com|instagr\\.am)/(?:explore/)?tags/([a-zA-Z0-9_%]+).*$",
    "apps": [
      {
        app: apps.keys.Instagram,
        "format": "instagram://tag?name=$1"
      }
    ]
  },
  {
    "title": "Open Location",
    "regex": "http(?:s)?://(?:www\\.)?(?:instagram\\.com|instagr\\.am)/explore/locations/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Instagram,
        "format": "instagram://location?id=$1"
      }
    ]
  },
  {
    "title": "Open Event",
    "regex": "http(?:s)?://(?:\\w+\\.)?facebook\\.com/events/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Facebook,
        "format": "fb://event?id=$1"
      }
    ]
  },
  {
    "title": "Open Group",
    "regex": "http(?:s)?://(?:\\w+\\.)?facebook\\.com/groups/(?:[^/]+/)?(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Facebook,
        "format": "fb://group?id=$1"
      }
    ]
  },
  {
    "title": "Open Group",
    "regex": "http(?:s)?://(?:\\w+\\.)?facebook\\.com/groups/(?!\\d+)[^/]+(/(?!\\d+)[^/]*)?$",
    "apps": [
      {
        app: apps.keys.Facebook,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('.*(fb://group(?:/)?\\\\?id=\\\\d+).*'); var results = regex.exec(res); var match = null; if (results != null && results.length > 1) { match = results[1] }; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Photo",
    "regex": "http(?:s)?://(?:\\w+\\.)?facebook\\.com/(?:photo\\.php.*(?:\\?|&)fbid=|[^/]+/photos/[^/]+/)(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Facebook,
        "format": "fb://photo/?id=$1"
      }
    ]
  },
  {
    "title": "View App",
    "regex": "http(?:s)?://apps\\.getpebble\\.com/(?:\\w+/)*application(?:s)?/([:hex:]+)",
    "apps": [
      {
        app: apps.keys.Pebble,
        "format": "pebble-3://appstore/$1"
      }
    ]
  },
  {
    "title": "Open Photo",
    "regex": "http(?:s?)://(?:www\\.)?500px\\.com/photo/(\\d+)(?:(?:/|\\?).*)?$",
    "apps": [
      {
        app: apps.keys['500px'],
        "format": "px://500px.com/photo/$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s?)://(?:www\\.)?500px\\.com/(?!photo)([0-9a-zA-Z_-]+)(?:/|\\?)?.*$",
    "apps": [
      {
        app: apps.keys['500px'],
        "format": "px://500px.com/$1"
      }
    ]
  },
  {
    "title": "Open Photo",
    "regex": "http(?:s)?://(?:m\\.|mobile\\.|www\\.)?flickr\\.com(?:/#|/browser/upgrade/\\?continue=)?/photos/([0-9a-zA-Z_@%-]+)/(\\d+)(?:(?:/|\\?).*)?$",
    "apps": [
      {
        app: apps.keys.Flickr,
        "format": "flickr://photos/$1/$2"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:m\\.|mobile\\.|www\\.)?flickr\\.com(?:/#|/browser/upgrade/\\?continue=)?/(?:photos|people)/([0-9a-zA-Z_@%-]+)(?:/|\\?|/\\?.*)?$",
    "apps": [
      {
        app: apps.keys.Flickr,
        "format": "flickr://flickr.com/photos/$1"
      }
    ]
  },
  {
    "title": "Open Photoset",
    "regex": "http(?:s)?://(?:m\\.|mobile\\.|www\\.)?flickr\\.com(?:/#|/browser/upgrade/\\?continue=)?/photos/([0-9a-zA-Z_@%-]+)/sets/(\\d+)(?:(?:/|\\?).*)?$",
    "apps": [
      {
        app: apps.keys.Flickr,
        "format": "flickr://photos/$1/sets/$2"
      }
    ]
  },
  {
    "title": "Open Group",
    "regex": "http(?:s)?://(?:m\\.|mobile\\.|www\\.)?flickr\\.com(?:/#|/browser/upgrade/\\?continue=)?/groups/([0-9a-zA-Z_@%-]+).*$",
    "apps": [
      {
        app: apps.keys.Flickr,
        "format": "flickr://groups/$1"
      }
    ]
  },
  {
    "title": "Open Photo",
    "regex": "http(?:s)?://(?:[a-zA-Z0-9_\\-\\.]+\\.)?vsco\\.co(?:m)?/(?:[a-zA-Z0-9_\\-\\.]+/)?media/(\\w+).*?$",
    "apps": [
      {
        app: apps.keys.VSCOcam,
        "format": "vsco://grid/$1"
      }
    ]
  },
  {
    "title": "Open Journal",
    "regex": "http(s)?://(?:[a-zA-Z0-9_\\-\\.]+\\.)?vsco\\.co(m)?/(?:[a-zA-Z0-9_\\-\\.]+/)?journal/\\w+.*$",
    "apps": [
      {
        app: apps.keys.VSCOcam,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(vsco://.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(s)?://(?:[a-zA-Z0-9_\\-\\.]+\\.)?vsco\\.co(m)?((/[a-zA-Z0-9_\\-\\.]+)?(/(collection|images)|(/)?\\?).*|/)?$",
    "apps": [
      {
        app: apps.keys.VSCOcam,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(vsco://.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Blog",
    "regex": "http(?:s?)://(?:www\\.)?([a-zA-Z0-9_\\-]+)\\.tumblr\\.com(?:/(?!(post|image|tagged|app)/).*|\\?.*|#.*)?$",
    "apps": [
      {
        app: apps.keys.Tumblr,
        "format": "tumblr://x-callback-url/blog?blogName=$1"
      }
    ]
  },
  {
    "title": "Open Blog",
    "regex": "http(?:s)?://(?!.*\\.tumblr\\.com)[a-zA-Z0-9-\\.]+/(?!post|image|tagged|app).*\\n.*\"(?:X-Tumblr-User)\":\"([a-zA-Z0-9_\\-]+)\".*$",
    "apps": [
      {
        app: apps.keys.Tumblr,
        "format": "tumblr://x-callback-url/blog?blogName=$1"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s?)://(?:www\\.)?([a-zA-Z0-9_\\-]+)\\.tumblr\\.com/(?!tagged|page)\\w+/(\\d+)(?:(?:/|\\?|#).*)?$",
    "apps": [
      {
        app: apps.keys.Tumblr,
        "format": "tumblr://x-callback-url/blog?blogName=$1&postID=$2"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://(?:www\\.)?([a-zA-Z0-9_\\-]+)\\.tumblr\\.com/tagged/(\\d+)(?:(?:/|\\?|#).*)?$",
    "apps": [
      {
        app: apps.keys.Tumblr,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('([a-zA-Z0-9_\\-]+)\\.tumblr\\.com/post/(\\\\d+)'); var match = regex.exec(res); var blogName = match[1]; var postID = match[2]; completionHandler('tumblr://x-callback-url/blog?blogName=' + blogName + '&postID=' + postID); }"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://(?!.*\\.tumblr\\.com)[a-zA-Z0-9-.]+/(?!tagged)\\w+/(\\d+).*\\n.*\"(?:X-Tumblr-User)\":\"([a-zA-Z0-9_\\-]+)\".*$",
    "apps": [
      {
        app: apps.keys.Tumblr,
        "format": "tumblr://x-callback-url/blog?blogName=$2&postID=$1"
      }
    ]
  },
  // {
  //   "title": "Open Journal",
  //   "regex": "http(?:s)?://app\\.gowander\\.co/web/v1/journals/(\\d+).*$",
  //   "apps": [
  //     {
  //       "app": "wander",
  //       "format": "comgowanderwander://journals/$1"
  //     }
  //   ]
  // },
  // {
  //   "title": "Open Moment",
  //   "regex": "http(?:s?)://(?:www\\.)?path\\.com/(?:p|moment)/.*$",
  //   "apps": [
  //     {
  //       "app": "path",
  //       "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('https://path.com/moment/([a-f0-9]+)'); var match = regex.exec(res)[1]; completionHandler('path://moments/' + match); }"
  //     }
  //   ]
  // },
  {
    "title": "Open Profile",
    "regex": "http(?:s?)://(?!(?:help|blog|engineering)\\.)(?:\\w+\\.)?pinterest\\.com/(?!pin|search|explore|categories|join)([a-zA-Z0-9-_]+)(/pins)?(?:/|/?\\?.*)?$",
    "apps": [
      {
        app: apps.keys.Pinterest,
        "format": "pinterest://user/$1"
      }
    ]
  },
  {
    "title": "Open Board",
    "regex": "http(?:s?)://(?!(?:help|blog|engineering)\\.)(?:\\w+\\.)?pinterest\\.com/(?!pin|search|explore|categories|discover)([a-zA-Z0-9-_]+)(?:/)(?!pins)([a-zA-Z0-9-_]+).*?$",
    "apps": [
      {
        app: apps.keys.Pinterest,
        "format": "pinterest://board/$1/$2"
      }
    ]
  },
  {
    "title": "Open Pin",
    "regex": "http(?:s?)://(?!(?:help|blog|engineering)\\.)(?:\\w+\\.)?pinterest\\.com/pin/(?!create)([a-zA-Z0-9_\\-]+).*?$",
    "apps": [
      {
        app: apps.keys.Pinterest,
        "format": "pinterest://pin/$1"
      }
    ]
  },
  {
    "title": "Open Search",
    "regex": "http(?:s)?://(?!(?:help|blog|engineering)\\.)(?:\\w+\\.)?pinterest\\.com/search/([^/|\\?]+).*q=([^&]+).*$",
    "apps": [
      {
        app: apps.keys.Pinterest,
        "format": "pinterest://search/$1/?q=$2"
      }
    ]
  },
  {
    "title": "Open Category",
    "regex": "http(?:s)?://(?!(?:help|blog|engineering)\\.)(?:\\w+\\.)?pinterest\\.com/(?:categories|explore)(/[^\\/\\?#]+)?.*?",
    "apps": [
      {
        app: apps.keys.Pinterest,
        "format": "pinterest://categories$1"
      }
    ]
  },
  {
    "title": "Open Topic",
    "regex": "http(?:s)?://(?:www\\.)?flipboard\\.com/topic/.*$",
    "apps": [
      {
        app: apps.keys.Flipboard,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(flipboard://.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Section",
    "regex": "http(?:s)?://(?:www\\.)?flipboard\\.com/(@\\w+(?:/\\w*(-\\w*)+)?|section/.*).*$",
    "apps": [
      {
        app: apps.keys.Flipboard,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(flipboard://.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  // {
  //   "title": "Open Link",
  //   "regex": "http(?:s)?://byte\\.co/~([a-zA-Z0-9_\\-]+).*$",
  //   "apps": [
  //     {
  //       "app": "byte",
  //       "format": "byte://$1"
  //     }
  //   ]
  // },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:\\w+\\.)?weibo\\.(?:com|cn)/(?:(?:u|d)/)?(\\w+)(?:/\\?.*|/)?$",
    "apps": [
      {
        app: apps.keys.Weibo,
        "format": "sinaweibo://userinfo?uid=$1"
      },
      {
        app: apps.keys.Moke,
        "format": "moke:///user?id=$1"
      }
    ]
  },
  // {
  //   "title": "Open Profile",
  //   "regex": "http(?:s)?://(?:\\w+\\.)?weibo\\.(?:com|cn)/(?:u/)?(\\d+)(?:/\\?.*|/)?$",
  //   "apps": [
  //     {
  //       app: apps.keys.Moke,
  //       "format": "moke:///user?id=$1"
  //     }
  //   ]
  // },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:\\w+\\.)?weibo\\.(?:com|cn)/(?:d/)?(?!\\d+)(\\w+)(?:/\\?.*|/)?$",
    "apps": [
      {
        app: apps.keys.Moke,
        "format": "moke:///user?domain=$1"
      }
    ]
  },
  {
    "title": "Open Status",
    "regex": "http(?:s)?://(?:\\w+\\.)?weibo\\.(?:com|cn)/(?:\\d+|status)/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Weibo,
        "format": "sinaweibo://detail?mblogid=$1"
      },
      {
        app: apps.keys.Moke,
        "format": "moke:///status?mid=$1"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://blog\\.sina\\.(?:com|cn|com\\.cn)/(?:.*/)?s/blog_(\\w+)\\.html.*$",
    "apps": [
      {
        app: apps.keys.SinaBlog,
        "format": "sinablog://blog.sina.com.cn?from=sinacn&jumptype=app&articleid=$1"
      }
    ]
  },
  {
    "title": "Open Venue",
    "regex": "http(?:s?)://(?:m\\.|www\\.|mobile\\.)?foursquare\\.com(?:/mobile)?/v(?:/\\S+)?/([:hex:]{24}).*$",
    "apps": [
      {
        app: apps.keys.Foursquare,
        "format": "foursquare://venues/$1"
      },
      {
        app: apps.keys.GoogleMaps,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://.*foursquare\\.com/v/(?:.*/)?(\\\\w+).*$'); var match = regex.exec(url); var venueIdentifier = match[1]; url = 'https://api.foursquare.com/v2/venues/' + venueIdentifier + '?v=20140921&client_id=Q5H4T3CZWKEKK3QKQKIIKWGISC1UKY2IXJ4BZCZXCLDQ0IMR&client_secret=J1TZFLIIZ4LK3I55AIFV3AW3Z4ICLAQIQ5KPNLTPT3ST03XH'; jsonRequest(url, function(res) { completionHandler('comgooglemaps://?q=' + res['response']['venue']['location']['lat'] + ',' + res['response']['venue']['location']['lng']); } ); }"
      },
      {
        app: apps.keys.AppleMaps,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://.*foursquare\\.com/v/(?:.*/)?(\\\\w+).*$'); var match = regex.exec(url); var venueIdentifier = match[1]; url = 'https://api.foursquare.com/v2/venues/' + venueIdentifier + '?v=20140921&client_id=Q5H4T3CZWKEKK3QKQKIIKWGISC1UKY2IXJ4BZCZXCLDQ0IMR&client_secret=J1TZFLIIZ4LK3I55AIFV3AW3Z4ICLAQIQ5KPNLTPT3ST03XH'; jsonRequest(url, function(res) { completionHandler('maps://maps.apple.com/maps?q=' + encodeURIComponent(res['response']['venue']['name']) + '&ll=' + res['response']['venue']['location']['lat'] + ',' + res['response']['venue']['location']['lng']); } ); }"
      },
      {
        app: apps.keys.Waze,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://.*foursquare\\.com/v/(?:.*/)?(\\\\w+).*$'); var match = regex.exec(url); var venueIdentifier = match[1]; url = 'https://api.foursquare.com/v2/venues/' + venueIdentifier + '?v=20140921&client_id=Q5H4T3CZWKEKK3QKQKIIKWGISC1UKY2IXJ4BZCZXCLDQ0IMR&client_secret=J1TZFLIIZ4LK3I55AIFV3AW3Z4ICLAQIQ5KPNLTPT3ST03XH'; jsonRequest(url, function(res) { completionHandler('waze://?q=' + encodeURIComponent(res['response']['venue']['name']) + '&ll=' + res['response']['venue']['location']['lat'] + ',' + res['response']['venue']['location']['lng']); } ); }"
      },
      {
        app: apps.keys.Transit,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://.*foursquare\\.com/v/(?:.*/)?(\\\\w+).*$'); var match = regex.exec(url); var venueIdentifier = match[1]; url = 'https://api.foursquare.com/v2/venues/' + venueIdentifier + '?v=20140921&client_id=Q5H4T3CZWKEKK3QKQKIIKWGISC1UKY2IXJ4BZCZXCLDQ0IMR&client_secret=J1TZFLIIZ4LK3I55AIFV3AW3Z4ICLAQIQ5KPNLTPT3ST03XH'; jsonRequest(url, function(res) { completionHandler('transit://directions?to=' + res['response']['venue']['location']['lat'] + ',' + res['response']['venue']['location']['lng']); } ); }"
      }
    ]
  },
  {
    "title": "Open List",
    "regex": "http(?:s)?://(m\\.|mobile\\.|www\\.)?foursquare\\.com/(?:(?:user/\\d+|[a-zA-Z0-9_%\\-]+)/list|list)/[a-zA-Z0-9_%\\-]+.*$",
    "apps": [
      {
        app: apps.keys.Foursquare,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(foursquare://.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Tip",
    "regex": "http(?:s)?://(?:m\\.|www\\.|mobile\\.)?foursquare\\.com(?:/mobile)?/item/([:hex:]{24})(?:/?)$",
    "apps": [
      {
        app: apps.keys.Foursquare,
        "format": "foursquare://tips/$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:m\\.|www\\.|mobile\\.)?foursquare\\.com(?:/mobile)?/(?:u|user)/(\\d+)(?:/?)$",
    "apps": [
      {
        app: apps.keys.Foursquare,
        "format": "foursquare://users/$1"
      }
    ]
  },
  {
    "title": "Open Search",
    "regex": "http(?:s)?://(?:m\\.|www\\.|mobile\\.)?foursquare\\.com(?:/mobile)?/explore.*q=([^&]+).*?$",
    "apps": [
      {
        app: apps.keys.Foursquare,
        "format": "foursquare://venues/explore?query=$1"
      }
    ]
  },
  {
    "title": "Open Check-in",
    "regex": "http(?:s)?://(?:www\\.)?swarmapp\\.com(?:/\\w+)?/checkin/([a-fA-F0-9]+\\?s=[a-zA-Z0-9_-]+)(?:.*)$",
    "apps": [
      {
        app: apps.keys.Swarm,
        "format": "swarm://checkins/$1"
      }
    ]
  },
  {
    "title": "Open Check-in",
    "regex": "http(?:s)?://(?:www\\.)?swarmapp\\.com/c/([a-zA-Z0-9_-]+)(?:.*)$",
    "apps": [
      {
        app: apps.keys.Swarm,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(swarm://.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Venue",
    "regex": "http(s)?://(www\\.)?swarmapp\\.com/((\\w+/)?checkin/|c/).*$",
    "apps": [
      {
        app: apps.keys.Foursquare,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('foursquare\\.com/v/(?:.*?/)?(\\\\w+)'); var match = regex.exec(res)[1]; completionHandler('foursquare://venues/' + match); }"
      }
    ]
  },
  {
    "title": "Open Listing",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?airbnb(?:\\.\\w+)+/(?:rooms|listings)/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Airbnb,
        "format": "airbnb://rooms/$1"
      }
    ]
  },
  {
    "title": "Open Check-in",
    "regex": "http(?:s)?://(?:www\\.)?(?:untappd\\.com/user/[a-zA-Z0-9_\\-\\.]+/checkin/|(?:untp\\.beer|untpd\\.it)/s/c)(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Untappd,
        "format": "untappd:///?checkin=$1"
      }
    ]
  },
  {
    "title": "Open Beer",
    "regex": "http(?:s)?://(?:www\\.)?untappd\\.com/b/[a-zA-Z0-9_\\-\\.]+/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Untappd,
        "format": "untappd:///?beer=$1"
      }
    ]
  },
  // {
  //   "title": "Check in with beer",
  //   "regex": "http(?:s)?://(?:www\\.)?untappd\\.com/b/[a-zA-Z0-9_\\-\\.]+/(\\d+).*$",
  //   "apps": [
  //     {
  //       "app": "tappdthat",
  //       "format": "tappdthat://checkin/?beerID=$1"
  //     }
  //   ]
  // },
  {
    "title": "Open Business",
    "regex": "http(?:s?)://(?:www\\.|mobile\\.|m\\.)?yelp(?:\\.[a-z]+)+/biz/([a-zA-Z0-9%\\-]+).*$",
    "apps": [
      {
        app: apps.keys.Yelp,
        "format": "yelp:///biz/$1"
      }
    ]
  },
  {
    "title": "Open Business Photos",
    "regex": "http(?:s?)://(?:www\\.|mobile\\.|m\\.)?yelp(?:\\.[a-z]+)+/biz_photos/([a-zA-Z0-9%\\-]+).*$",
    "apps": [
      {
        app: apps.keys.Yelp,
        "format": "yelp:///biz/photos?biz_id=$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://((?:www\\.)?google(?:\\.\\w+)+/maps/.*)",
    "apps": [
      {
        app: apps.keys.GoogleMaps,
        "format": "comgooglemapsurl://$1"
      }
    ],
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://((?:www\\.)?google(?:\\.\\w+)+/maps/place/([^/]+)/(?:(?:@([^/]+)|(?!data)([^/]+)))?.*)$",
    "apps": [
      {
        app: apps.keys.AppleMaps,
        "format": "maps://maps.apple.com/maps?q=$2&ll=$3&address=$4"
      },
      {
        app: apps.keys.Waze,
        "format": "waze://?q=$2&ll=$3"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(maps\\.google(?:\\.\\w+)+/(?!maps/place/.*).*(\\?.*))",
    "apps": [
      {
        app: apps.keys.GoogleMaps,
        "format": "comgooglemapsurl://$1"
      },
      {
        app: apps.keys.AppleMaps,
        "format": "maps://maps.apple.com/maps$2"
      },
      {
        app: apps.keys.Waze,
        "format": "waze://$2"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://maps\\.apple\\.com/(?:maps)?\\?(.*)",
    "apps": [
      {
        app: apps.keys.AppleMaps,
        "format": "maps://maps.apple.com/maps?$1"
      },
      {
        app: apps.keys.GoogleMaps,
        "script": "function getParameterByName(url, name) { name = name.replace(new RegExp('[\\[]'), '\\\\[').replace(new RegExp('[\\]]'), '\\\\]'); var regex = new RegExp('[\\\\?&]' + name + '=([^&#]*)'); results = regex.exec(url); return results === null ? '' : decodeURIComponent(results[1].replace(new RegExp('\\\\+', 'g'), ' ')); } function process(url, completionHandler) { var appleAddress = getParameterByName(url, 'address'); if (appleAddress == null || appleAddress.length == 0) { appleAddress = getParameterByName(url, 'hnear'); } var appleLL = getParameterByName(url, 'll'); if (!appleLL) { appleLL = getParameterByName(url, 'sll'); } var appleQ = getParameterByName(url, 'q');  var googleQ= ''; var googleCenter = null; if (appleAddress != null && appleAddress.length > 0) { googleQ = appleAddress; } else if (appleQ != null) { googleQ = appleQ; } if (appleLL != null && appleLL.length > 0) { googleCenter = appleLL; } var query = ''; if (googleQ.length > 0) { query = 'q=' + encodeURIComponent(googleQ); } if (googleCenter.length > 0) { if (query.length > 0) { query = query + '&'; } query = query + 'center=' + googleCenter; } var url = 'comgooglemaps://?' + query; completionHandler(url); }"
      },
      {
        app: apps.keys.AppleMaps,
        "script": "function getParameterByName(url, name) { name = name.replace(new RegExp('[\\[]'), '\\\\[').replace(new RegExp('[\\]]'), '\\\\]'); var regex = new RegExp('[\\\\?&]' + name + '=([^&#]*)'); results = regex.exec(url); return results === null ? '' : decodeURIComponent(results[1].replace(new RegExp('\\\\+', 'g'), ' ')); } function process(url, completionHandler) { var appleAddress = getParameterByName(url, 'address'); if (appleAddress == null || appleAddress.length == 0) { appleAddress = getParameterByName(url, 'hnear'); } var appleLL = getParameterByName(url, 'll'); if (!appleLL) { appleLL = getParameterByName(url, 'sll'); } var appleQ = getParameterByName(url, 'q');  var googleQ= ''; var googleCenter = null; if (appleQ != null) { googleQ = appleQ; } if (appleAddress != null && appleAddress.length > 0) { if (googleQ.length > 0) { googleQ = googleQ + ','; } googleQ = googleQ + appleAddress; } if (appleLL != null && appleLL.length > 0) { googleCenter = appleLL; } var query = ''; if (googleQ.length > 0) { query = 'q=' + encodeURIComponent(googleQ); } if (googleCenter.length > 0) { if (query.length > 0) { query = query + '&'; } query = query + 'll=' + googleCenter; } var url = 'waze://?' + query; completionHandler(url); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?waze\\.to(?:/)?\\?([^#]+).*$",
    "apps": [
      {
        app: apps.keys.Waze,
        "format": "waze://?$1"
      },
      {
        app: apps.keys.AppleMaps,
        "format": "maps://maps.apple.com/maps?$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?waze\\.to(?:/)?\\?(?:.+&)?ll=([^&]+).*$",
    "apps": [
      {
        app: apps.keys.GoogleMaps,
        "format": "comgooglemaps://?q=$1&center=$1"
      }
    ],
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)*(?:maps\\.(?:apple|google)(?:.\\w+)+|google(?:\\.\\w+)+/maps|waze\\.to).*(?:ll=|@)([\\-0-9\\.]+,[\\-0-9\\.]+).*?$",
    "apps": [
      {
        app: apps.keys.Transit,
        "format": "transit://directions?to=$1"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?(?:youtu\\.be/|youtube\\.com/(?:embed/|.*(?:\\?|&)v=))([a-zA-Z0-9_\\-]+).*?((\\?|&)(start|t)=\\w+)?.*?$",
    "apps": [
      {
        app: apps.keys.Youtube,
        "format": "youtube://$1$2"
      },
      {
        app: apps.keys.ProTube,
        "format": "protube://video/$1"
      },
      {
        app: apps.keys.YouPlayer,
        "format": "youplayer://youtube.com/watch?v=$1"
      },
      {
        app: apps.keys.CornerTube,
        "format": "cornertube://video/https://youtu.be/$1"
      },
      {
        app: apps.keys.PipTube,
        "format": "piptube://openvideo?url=https://www.youtube.com/watch?v=$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?youtube\\.com/user/([\\w]+).*$",
    "apps": [
      {
        app: apps.keys.Youtube,
        "format": "youtube://youtube.com/user/$1"
      },
      {
        app: apps.keys.ProTube,
        "format": "protube://user/$1"
      },
      {
        app: apps.keys.YouPlayer,
        "format": "youplayer://youtube.com/user/$1"
      }
    ]
  },
  {
    "title": "Open Channel",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?youtube\\.com/channel/([a-zA-Z0-9_\\-]+).*$",
    "apps": [
      {
        app: apps.keys.Youtube,
        "format": "youtube://youtube.com/channel/$1"
      },
      {
        app: apps.keys.ProTube,
        "format": "protube://channel/$1"
      },
      {
        app: apps.keys.YouPlayer,
        "format": "youplayer://youtube.com/channel/$1"
      }
    ]
  },
  {
    "title": "Open Playlist",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?youtube\\.com/playlist.*list=([a-zA-Z0-9_\\-]+).*?$",
    "apps": [
      {
        app: apps.keys.Youtube,
        "format": "youtube://youtube.com/playlist?list=$1"
      },
      {
        app: apps.keys.ProTube,
        "format": "protube://playlist/$1"
      },
      {
        app: apps.keys.YouPlayer,
        "format": "youplayer://youtube.com/playlist?list=$1"
      }
    ]
  },
  {
    "title": "Open Broadcast",
    "regex": "http(?:s?)://(?:www\\.)?(?:periscope|pscp)\\.tv/[^/]+/([^/|\\?]+)(?:/|/?\\?.*)?$",
    "apps": [
      {
        app: apps.keys.Periscope,
        "format": "pscp://broadcast/$1"
      }
    ]
  },
  {
    "title": "Open Stream",
    "regex": "http(?:s)?://(?:[a-zA-Z0-9_\\-]+\\.)*twitch\\.tv/(?!directory)(\\w+)(?!.*/v/\\d+).*$",
    "apps": [
      {
        app: apps.keys.Twitch,
        "format": "twitch://stream/$1"
      }
    ]
  },
  {
    "title": "Open Game Directory",
    "regex": "http(?:s)?://(?:[a-zA-Z0-9_\\-]+\\.)*twitch\\.tv/directory/game/([a-zA-Z0-9_\\-%]+).*$",
    "apps": [
      {
        app: apps.keys.Twitch,
        "format": "twitch://game/$1"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:[a-zA-Z0-9_\\-]+\\.)*twitch\\.tv/(?!directory)(?:\\w+)/v/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Twitch,
        "format": "twitch://video/v$1"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://(?:www\\.)?flipagram\\.com/f/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Flipagram,
        "format": "flipagram://flipagram/$1"
      }
    ]
  },
  {
    "title": "Open Dubsmash",
    "regex": "http(?:s)?://api\\.dubsmash\\.com/goto/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Dubsmash,
        "format": "dubsmash://1/snip/$1"
      }
    ]
  },
  {
    "title": "Open File",
    "regex": "http(?:s?)://(?:www\\.)?dropbox\\.com/s/\\w+.*$",
    "apps": [
      {
        app: apps.keys.Dropbox,
        "script": "function process(url, completionHandler) { completionHandler('dbapi-6:/1/viewLink?url=' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Open Folder",
    "regex": "http(?:s?)://(?:www\\.)?dropbox\\.com/sh/\\w+.*$",
    "apps": [
      {
        app: apps.keys.Dropbox,
        "script": "function process(url, completionHandler) { completionHandler('dbapi-6:/1/viewLink?url=' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "(http(?:s?)://drive\\.google\\.com/(?:open|file|folder).*)$",
    "apps": [
      {
        app: apps.keys.GoogleDrive,
        "format": "googledrive://$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://m\\.box\\.com/shared_item/(.*)$",
    "apps": [
      {
        app: apps.keys.Box,
        "format": "boxopenshared://shared_item?url=$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:app\\.)?box\\.com/s/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Box,
        "format": "boxopenshared://shared_item?url=https%3A%2F%2Fapp.box.com%2Fs%2F$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:www\\.)?vimeo\\.com/([a-zA-Z](\\w+))(?:/)?(?:\\?.*)?$",
    "apps": [
      {
        app: apps.keys.Vimeo,
        "format": "vimeo://app.vimeo.com/users/$1"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:www\\.|player\\.)?vimeo\\.com/(?:video/)?(\\d+)(?:/)?.*$",
    "apps": [
      {
        app: apps.keys.Vimeo,
        "format": "vimeo://app.vimeo.com/videos/$1"
      }
    ]
  },
  {
    "title": "Open Channel",
    "regex": "http(?:s)?://(?:www\\.)?vimeo\\.com/channels/(\\w+)(?:/)?(?:\\?.*)?$",
    "apps": [
      {
        app: apps.keys.Vimeo,
        "format": "vimeo://app.vimeo.com/channels/$1"
      }
    ]
  },
  {
    "title": "Open Category",
    "regex": "http(?:s)?://(?:www\\.)?vimeo\\.com/categories/(\\w+)(?:/)?(?:\\?.*)?$",
    "apps": [
      {
        app: apps.keys.Vimeo,
        "format": "vimeo://app.vimeo.com/categories/$1"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:\\w+\\.)?youku\\.com/v_show/id_(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Youku,
        "format": "youku://play?vid=$1"
      },
      {
        "app": apps.keys.YoukuHD,
        "format": "youkuhd://play?vid=$1"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:www\\.)?bilibili\\.com/(?:.*/)?video/av(\\d+).*$",
    "apps": [
      {
        app: apps.keys.bilibili,
        "format": "bilibili://?av=$1"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:www\\.)?hulu(?:\\.\\w+)+/watch/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Hulu,
        "format": "hulu://w/$1"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:www\\.)vevo\\.com/watch/(?:[a-zA-Z0-9()\\-_%]+/)*(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Vevo,
        "format": "vevo://video/$1"
      }
    ]
  },
  {
    "title": "Open Track",
    "regex": "http(?:s?)://(?:play\\.|open\\.|www\\.)?spotify\\.com/track/([:alnum:]+).*$",
    "apps": [
      {
        app: apps.keys.Spotify,
        "format": "spotify:track:$1"
      }
    ]
  },
  {
    "title": "Open Album",
    "regex": "http(?:s?)://(?:play\\.|open\\.|www\\.)?spotify\\.com/album/([:alnum:]+).*$",
    "apps": [
      {
        app: apps.keys.Spotify,
        "format": "spotify:album:$1"
      }
    ]
  },
  {
    "title": "Open Playlist",
    "regex": "http(?:s?)://(?:play\\.|open\\.|www\\.)?spotify\\.com/user/(\\w+)/playlist/([:alnum:]+).*$",
    "apps": [
      {
        app: apps.keys.Spotify,
        "format": "spotify:user:$1:playlist:$2"
      }
    ]
  },
  {
    "title": "Open User",
    "regex": "http(?:s?)://(?:play\\.|open\\.|www\\.)?spotify\\.com/user/(\\w+)(?!.*/playlist.*).*$",
    "apps": [
      {
        app: apps.keys.Spotify,
        "format": "spotify:user:$1"
      }
    ]
  },
  {
    "title": "Open Artist",
    "regex": "http(?:s?)://(?:play\\.|open\\.|www\\.)?spotify\\.com/artist/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Spotify,
        "format": "spotify:artist:$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(s?)://(www\\.|m\\.)?soundcloud\\.com/[^/]+(/[^/]+)?",
    "apps": [
      {
        app: apps.keys.SoundCloud,
        "script": "function process(url, completionHandler) { url = 'https://api.soundcloud.com/resolve?client_id=27fcfadd796648a26072b6041ff5bf74&url=' + url; jsonRequest(url, function(result) { var appURL = null; if (result['kind'] === 'track') { appURL = 'soundcloud://sounds:' + result['id']; } else if (result['kind'] === 'user') { appURL = 'soundcloud://users:' + result['id']; } else if (result['kind'] === 'playlist') { appURL = 'soundcloud://sets:' + result['id']; } completionHandler(appURL); } ); }"
      }
    ]
  },
  {
    "title": "Open Track",
    "regex": "http(?:s)?://(?:www\\.)?hypem\\.com/track/([:alnum:]+).*$",
    "apps": [
      {
        app: apps.keys.HappyMachine,
        "format": "hypem://track/$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:www\\.)?hypem\\.com/(?!track|popular|twitter|tags)(\\w+).*$",
    "apps": [
      {
        app: apps.keys.HappyMachine,
        "format": "hypem://playlist/?key=PlaylistFriend&section=users&username=$1"
      }
    ]
  },
  {
    "title": "Open Playlist",
    "regex": "http(?:s)?://(?:www\\.)?hypem\\.com/(?!track|popular|latest|twitter|tags)(\\w+).*$",
    "apps": [
      {
        app: apps.keys.HappyMachine,
        "format": "hypem://playlist/?key=PlaylistFriend&section=users&username=$1"
      }
    ]
  },
  {
    "title": "Open Station",
    "regex": "http(?:s)?://(?:www\\.)?tunein\\.com/(?:station(?:/)?\\?StationId=|radio/(?:\\S*-)*s)(\\d+).*$",
    "apps": [
      {
        app: apps.keys.TuneIn,
        "format": "tunein://?profile/s$1"
      }
    ]
  },
  {
    "title": "Add Podcast",
    "regex": "http(?:s)?://(?:www\\.)?(overcast\\.fm/\\+.+)$",
    "apps": [
      {
        app: apps.keys.Overcast,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(overcast:///\\\\d+)'); var match = regex.exec(res)[1]; completionHandler(match); }"
      },
      {
        app: apps.keys.Castro,
        "format": "castro://subscribe/$1"
      },
      {
        app: apps.keys.PocketCasts,
        "format": "pktc://subscribe/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(s)?://pca.st/\\w+$",
    "apps": [
      {
        app: apps.keys.PocketCasts,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('.*(pktc://.*?)\\'.*'); var results = regex.exec(res); var match = null; if (results != null && results.length > 1) { match = results[1] }; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open List",
    "regex": "http(?:s)?://lists\\.pocketcasts\\.com/([^/]*).*$",
    "apps": [
      {
        app: apps.keys.PocketCasts,
        "format": "pktc://sharelist/lists.pocketcasts.com/$1"
      }
    ]
  },
  {
    "title": "Add iTunes Podcast",
    "regex": "http(?:s)?://(?:geo\\.)?(itunes\\.apple\\.com/(?:.*/)*podcast/(?:.*/)?id\\d+.*)$",
    "apps": [
      {
        app: apps.keys.Overcast,
        "script": "function process(url, completionHandler) { completionHandler('overcast://x-callback-url/add?url=' + encodeURIComponent(url)); }"
      },
      {
        app: apps.keys.Castro,
        "format": "castro://subscribe/$1"
      },
      {
        app: apps.keys.PocketCasts,
        "format": "pktc://subscribe/$1"
      }
    ]
  },
  {
    "title": "Open Product",
    "regex": "http(?:s)?://(?:www\\.)?producthunt\\.com/(?:posts|tech|games|podcasts|books|)/([a-zA-Z0-9_\\-%]+).*$",
    "apps": [
      {
        app: apps.keys.ProductHunt,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('producthunt://post/(\\\\d+)'); var match = regex.exec(res)[1]; completionHandler('producthunt://post/' + match); }"
      }
    ]
  },
  {
    "title": "Open User",
    "regex": "http(?:s)?://(?:www\\.)?producthunt\\.com/@(\\w+)(?!.*/collections/.*)(.*)$",
    "apps": [
      {
        app: apps.keys.ProductHunt,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('producthunt://user/(\\\\d+)'); var match = regex.exec(res)[1]; completionHandler('producthunt://user/' + match); }"
      }
    ]
  },
  {
    "title": "Open Collection",
    "regex": "http(?:s)?://(?:www\\.)?producthunt\\.com/@(?:\\w+)/collections/.*$",
    "apps": [
      {
        app: apps.keys.ProductHunt,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('producthunt://collection/(\\\\d+)'); var match = regex.exec(res)[1]; completionHandler('producthunt://collection/' + match); }"
      }
    ]
  },
  {
    "title": "Open Talk",
    "regex": "http(?:s?)://(?:www\\.)?ted\\.com/talks/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Ted,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(ted://talks/\\\\d+)'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Subreddit",
    "regex": "http(?:s)?://(?:\\w+\\.)?reddit\\.com/r/([a-zA-Z0-9_\\-%]+)(?:/(\\?.*)?$|\\?.*$|/$)?$",
    "apps": [
      {
        app: apps.keys.AlienBlue,
        "format": "alienblue://r/$1"
      }
    ]
  },
  {
    "title": "Open Thread",
    "regex": "http(?:s)?://(?:\\w+\\.)?reddit\\.com/(r/[a-zA-Z0-9_\\-%]+/(?:comments|info)/\\w+.*)$",
    "apps": [
      {
        app: apps.keys.AlienBlue,
        "format": "alienblue://thread/$1"
      }
    ]
  },
  {
    "title": "Open Thread",
    "regex": "http(?:s)?://(?:\\w+\\.)?reddit\\.com/(?:comments|info)/\\w+.*$",
    "apps": [
      {
        app: apps.keys.AlienBlue,
        "script": "function process(url, completionHandler) { var regex = new RegExp('reddit.com/(?:comments|info)/(\\\\w+).*?$'); var id = regex.exec(url)[1]; url = 'https://api.reddit.com/info/' + id; jsonRequest(url, function(res) { var permalink = res[0]['data']['children'][0]['data']['permalink']; completionHandler('alienblue://thread' + permalink); } ); }"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://(?:www\\.)?theguardian\\.(?:com|co\\.uk)/(.*)$",
    "apps": [
      {
        app: apps.keys.TheGuardian,
        "format": "gnmguardian://$1"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://(?:www\\.)?buzzfeed\\.com/(\\w+/[a-zA-Z0-9_\\-%]+).*$",
    "apps": [
      {
        app: apps.keys.BuzzFeed,
        "format": "buzzfeed://buzz/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://((?:\\w+\\.)?(reddit\\.com/.*))",
    "apps": [
      {
        app: apps.keys.Reddit,
        "format": "reddit://$1"
      },
      {
        app: apps.keys.narwhal,
        "script": "function process(url, completionHandler) { completionHandler('narwhal://open-url/' + encodeURIComponent(url.split('?')[0])); }"
      },
      {
        app: apps.keys.amrc,
        "format": "amrc://$2"
      },
      {
        "app": apps.keys.Beam,
        "format": "beamapp://$2"
      },
      {
        app: apps.keys.Readder,
        "format": "readder://open-url/https://$2"
      }
    ]
  },
  {
    "title": "Open Document",
    "regex": "http(?:s)?://(?:\\w+\\.)?scribd\\.com(?:/\\w+)*/(?:doc|embeds)/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Scribd,
        "format": "iscribd://doc?id=$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?stumbleupon\\.com/(?:su|to/s)/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.StumbleUpon,
        "format": "stumbleupon://urlid/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?medium\\.com/(.*)$",
    "apps": [
      {
        app: apps.keys.Medium,
        "format": "medium://$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?!.*medium\\.com)[a-zA-Z0-9\\-.]+/(?!\\?.*)(.+)\\n.*\"(?:x-powered-by)\":\"(?:kale|medium)\".*$",
    "apps": [
      {
        app: apps.keys.Medium,
        "format": "medium://p/$1"
      }
    ]
  },
  {
    "title": "Open Publication",
    "regex": "http(?:s)?://(?!.*medium\\.com)[a-zA-Z0-9\\-.]+(?:/(?:\\?.*)?)?\\n.*\"(?:x-powered-by)\":\"(?:kale|medium)\".*$",
    "apps": [
      {
        app: apps.keys.Medium,
        "script": "function process(url, completionHandler) { var res = $http.sync(url.split('\\n')[0]); var regex = RegExp('(medium://.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Product",
    "regex": "http(?:s)?://(?:\\w+\\.)*(?:amazon|amzn)((?:\\.\\w+)+)/(?:.*?/)?(?:(?:(?:dp|gp)(?:/aw/d)?)|product|.*/ASIN)/([a-zA-Z0-9]+).*$",
    "apps": [
      {
        app: apps.keys.Amazon,
        "format": "com.amazon.mobile.shopping://amazon$1/products/$2"
      },
      {
        app: apps.keys.Associate,
        "script": "function process(url, completionHandler) { completionHandler('associate://x-callback-url/convert?url=' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Open Deal",
    "regex": "http(?:s)?://(?:www\\.)groupon(?:\\.\\w+)+/deals/.*$",
    "apps": [
      {
        app: apps.keys.Groupon,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('ios-app://352683833/groupon/dispatch/(.*?)\"'); var match = regex.exec(res)[1]; completionHandler('groupon:///dispatch/' + match); }"
      }
    ]
  },
  {
    "title": "Open Product",
    "regex": "http(?:s)?://(?:\\w+\\.)*aliexpress\\.com/(?:\\w+/)?item(?:/[^/]+)?/(\\d+)\\.html.*$",
    "apps": [
      {
        app: apps.keys.AliExpress,
        "format": "aliexpress://product/detail?productId=$1"
      },
      {
        app: apps.keys.AliExpressHD,
        "format": "aliexpresshd://product/detail?productId=$1"
      }
    ]
  },
  {
    "title": "Open Product",
    "regex": "http(?:s)?://(?:\\w+\\.)?alibaba\\.com/product(?:-detail)?/(?:[a-zA-Z0-9_\\-]*_)?(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Alibaba,
        "format": "enalibaba://detail/?id=$1"
      }
    ]
  },
  {
    "title": "Open Product",
    "regex": "http(?:s)?://(?:www\\.|rover\\.|m\\.)?ebay(?:\\.\\w+)+/itm/(?:[0-9A-Za-z_-]+/)?(\\d+)(?:/|\\?)?(?:.*)$",
    "apps": [
      {
        app: apps.keys.eBay,
        "format": "ebay://launch?itm=$1"
      }
    ]
  },
  {
    "title": "Open Project",
    "regex": "http(?:s)?://((?:www\\.)?kickstarter\\.com/projects/[a-zA-Z0-9_%-]+/[a-zA-Z0-9_%-]+).*?$",
    "apps": [
      {
        app: apps.keys.Kickstarter,
        "format": "ksr://$1"
      }
    ]
  },
  {
    "title": "Open Item",
    "regex": "http(?:s?)://(?:www\\.|m\\.)?etsy\\.com/(?:.+/)*listing/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Etsy,
        "format": "etsy://listing/$1"
      }
    ]
  },
  {
    "title": "Open Product",
    "regex": "http(?:s)?://(?:www\\.)?fancy\\.com/things/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Fancy,
        "format": "fancy://things/$1"
      }
    ]
  },
  {
    "title": "Open Project",
    "regex": "http(?:s)?://(?:www\\.)?behance\\.net/gallery/(?:(?:[a-zA-Z0-9_\\-\\.]/)+)?(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Behance,
        "format": "behance://project/$1"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://(?:www\\.)?weheartit\\.com/entry/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.WeHeartIt,
        "format": "whi://entry/$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:www\\.)?weheartit\\.com/(?!entry)([a-zA-Z0-9_\\-]+)(?!.*/collections/.+).*$$",
    "apps": [
      {
        app: apps.keys.WeHeartIt,
        "format": "whi://user/$1"
      }
    ]
  },
  {
    "title": "Open Collection",
    "regex": "http(?:s)?://(?:www\\.)?weheartit\\.com/(?!entry)((?:[a-zA-Z0-9_\\-]+)/collections/(?:[a-zA-Z0-9_\\-]+)).*$",
    "apps": [
      {
        app: apps.keys.WeHeartIt,
        "format": "whi://$1"
      }
    ]
  },
  {
    "title": "Open Comic",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?xkcd\\.com/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.XkcdHD,
        "format": "xkcd://$1"
      }
    ]
  },
  {
    "title": "Open Frontback",
    "regex": "http(s?)://(www\\.)?(frontback\\.me/p|frntb\\.ac)/[a-zA-Z0-9]+(\\S*)$",
    "apps": [
      {
        app: apps.keys.Frontback,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(frontback://posts/\\\\d+)'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Event",
    "regex": "http(?:s)?://(?:www\\.)?eventbrite(?:\\.\\w+)+/e(?:vent)?/(?:[a-zA-Z0-9_]+-)*(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Eventbrite,
        "format": "com-eventbrite-attendee:event/$1"
      }
    ]
  },
  {
    "title": "Open Event",
    "regex": "http(?:s?)://(?:m\\.|mobile\\.|www\\.)?meetup\\.com/([a-zA-Z0-9_\\-%]+)(?:/.*)?/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Meetup,
        "format": "meetup://events/$2"
      },
      {
        app: apps.keys.GoogleMaps,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s?)://(?:m\\\\.|mobile\\\\.|www\\\\.)?meetup\\\\.com/([a-zA-Z0-9_\\\\-%]+)(?:/.*)?/(\\\\d+).*$'); var match = regex.exec(url); var groupIdentifier = match[1]; var eventIdentifier = match[2]; url = 'https://api.meetup.com/' + groupIdentifier + '/events/' + eventIdentifier; jsonRequest(url, function(res) { if (res['venue'] != null && res['venue']['name'] != null && res['venue']['lat'] != null & res['venue']['lon'] != null) { completionHandler('comgooglemaps://?q=' + res['venue']['lat'] + ',' + res['venue']['lon']); } else { completionHandler(null); } } ); }"
      },
      {
        app: apps.keys.AppleMaps,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s?)://(?:m\\\\.|mobile\\\\.|www\\\\.)?meetup\\\\.com/([a-zA-Z0-9_\\\\-%]+)(?:/.*)?/(\\\\d+).*$'); var match = regex.exec(url); var groupIdentifier = match[1]; var eventIdentifier = match[2]; url = 'https://api.meetup.com/' + groupIdentifier + '/events/' + eventIdentifier; jsonRequest(url, function(res) { if (res['venue'] != null && res['venue']['name'] != null && res['venue']['lat'] != null & res['venue']['lon'] != null) { completionHandler('maps://maps.apple.com/maps?q=' + encodeURIComponent(res['venue']['name']) + '&ll=' + res['venue']['lat'] + ',' + res['venue']['lon']); } else { completionHandler(null); } } ); }"
      },
      {
        app: apps.keys.Waze,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s?)://(?:m\\\\.|mobile\\\\.|www\\\\.)?meetup\\\\.com/([a-zA-Z0-9_\\\\-%]+)(?:/.*)?/(\\\\d+).*$'); var match = regex.exec(url); var groupIdentifier = match[1]; var eventIdentifier = match[2]; url = 'https://api.meetup.com/' + groupIdentifier + '/events/' + eventIdentifier; jsonRequest(url, function(res) { if (res['venue'] != null && res['venue']['name'] != null && res['venue']['lat'] != null & res['venue']['lon'] != null) { completionHandler('waze://?q=' + encodeURIComponent(res['venue']['name']) + '&ll=' + res['venue']['lat'] + ',' + res['venue']['lon']); } else { completionHandler(null); } } ); }"
      },
      {
        app: apps.keys.Transit,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s?)://(?:m\\\\.|mobile\\\\.|www\\\\.)?meetup\\\\.com/([a-zA-Z0-9_\\\\-%]+)(?:/.*)?/(\\\\d+).*$'); var match = regex.exec(url); var groupIdentifier = match[1]; var eventIdentifier = match[2]; url = 'https://api.meetup.com/' + groupIdentifier + '/events/' + eventIdentifier; jsonRequest(url, function(res) { if (res['venue'] != null && res['venue']['lat'] != null & res['venue']['lon'] != null) { completionHandler('transit://directions?to=' + res['venue']['lat'] + ',' + res['venue']['lon']); } else { completionHandler(null); } } ); }"
      }
    ]
  },
  {
    "title": "Open Doodle",
    "regex": "http(?:s)?://(?:www\\.)?doodle\\.com/(?:poll/)?([a-z0-9]{16}).*$",
    "apps": [
      {
        app: apps.keys.Doodle,
        "format": "doodle://?pollId=$1"
      }
    ]
  },
  {
    "title": "Open Event",
    "regex": "http(?:s)?://(?:www\\.|m\\.|mobile\\.)stubhub\\.com/(?:\\S+/)*(?:\\w+-)*(\\d+).*$",
    "apps": [
      {
        app: apps.keys.StubHub,
        "format": "stubhub://stubhub.com/?event_id=$1"
      }
    ]
  },
  {
    "title": "Open Event",
    "regex": "http(s)?://(www\\.)?goldstar\\.com/events/[a-zA-Z0-9\\-_]+/[a-zA-Z0-9\\-_]+.*$",
    "apps": [
      {
        app: apps.keys.Goldstar,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(goldstar://.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:touch\\.)?(?:\\w+\\.)?linkedin\\.com/(?:(?:.*(?:/|#))?profile/(?:view?.*id=)?(\\d+)|in/([^/]+)).*$",
    "apps": [
      {
        app: apps.keys.LinkedIn,
        "format": "linkedin://profile/$1$2"
      }
    ]
  },
  {
    "title": "Open Title",
    "regex": "http(?:s)?://(?:www\\.|m\\.)imdb\\.com/title/(tt\\d+).*$",
    "apps": [
      {
        app: apps.keys.IMDb,
        "format": "imdb:///title/$1"
      }
    ]
  },
  {
    "title": "Open Chart",
    "regex": "http(?:s)?://(?:www\\.|m\\.)imdb\\.com/chart/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.IMDb,
        "format": "imdb:///chart/$1"
      }
    ]
  },
  {
    "title": "Open Name",
    "regex": "http(?:s)?://(?:www\\.|m\\.)imdb\\.com/name/(nm\\d+).*$",
    "apps": [
      {
        app: apps.keys.IMDb,
        "format": "imdb:///name/$1"
      }
    ]
  },
  {
    "title": "Open Activity",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?strava\\.com/activities/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Strava,
        "format": "strava://activities/$1"
      }
    ]
  },
  {
    "title": "Open Run",
    "regex": "http(?:s)?://(?:www\\.)?runkeeper\\.com/(?:user/\\w+/activity/\\d+|activity/?.*)",
    "apps": [
      {
        app: apps.keys.Runkeeper,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(runkeeper://\\\\?view=activity&tripuuid=(\\\\w+|-)+)'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://((?!www\\.)(?:([a-zA-Z0-9\\-]+)\\.)(?:\\w+\\.)*wikipedia\\.(?:com|org)/(?:[^/]+/)?(.*))$",
    "apps": [
      {
        app: apps.keys.Wikipedia,
        "format": "wikipedia-official://$1"
      },
      {
        app: apps.keys.Viki,
        "format": "v-for-wiki://article/$2/$3"
      },
      {
        app: apps.keys.WikiLinks,
        "script": "function process(url, completion) { url = url.replace(new RegExp('\\.org/[^/]+/'), '.org/wiki/'); completion('wikilinks://openwikipediaurl?url=' + encodeURIComponent(btoa(url))); }"
      },
      {
        app: apps.keys.Wikipanion,
        "format": "wplink://$2.wikipedia.org/wiki/$3"
      },
      {
        app: apps.keys.Wikiwand,
        "format": "wikiwand-article://$2/$3"
      },
      {
        app: apps.keys.Wonder,
        "format": "wonder://$1"
      },
      {
        app: apps.keys.Curiosity,
        "script": "function process(url, completionHandler) { var regex = new RegExp('(http(?:s)?://((?!www\\\\.)(?:([a-zA-Z0-9\\\\-]+)\\\\.)(?:\\\\w+\\\\.)*wikipedia\\\\.(?:com|org)/(?:[^/]+/)?(.*)))$'); var match = regex.exec(url); var locale = match[3]; var title = match[4]; url = 'https://' + locale + '.wikipedia.org/w/api.php?action=query&format=json&titles=' + title; jsonRequest(url, function(res) { var pageid = Object.keys(res['query']['pages'])[0]; completionHandler('curiosity://p/' + locale + '/' + pageid); } ); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?tamper\\.io/(?:curiosity|inquire)/share.*(?:(?:\\?|&)p=(\\d+).*&l=([a-zA-Z0-9\\-_]+)|(?:\\?|&)l=([a-zA-Z0-9\\-_]+).*&p=(\\d+)).*$",
    "apps": [
      {
        app: apps.keys.Wikipedia,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://(?:www\\\\.)?tamper\\\\.io/(?:curiosity|inquire)/share.*(?:(?:\\\\?|&)p=(\\\\d+).*&l=([a-zA-Z0-9\\\\-_]+)|(?:\\\\?|&)l=([a-zA-Z0-9\\\\-_]+).*&p=(\\\\d+)).*$'); var match = regex.exec(url); var locale = null; if (match[2] != null) { locale = match[2] } else if (match[3] != null) { locale = match[3]; } var pageid = null; if (match[1] != null) { pageid = match[1] } else if (match[4] != null) { pageid = match[4]; } url = 'https://' + locale + '.wikipedia.org/w/api.php?action=query&format=json&pageids=' + pageid; jsonRequest(url, function(res) { var key = Object.keys(res['query']['pages'])[0]; var title = res['query']['pages'][key]['title']; completionHandler('wikipedia-official://' + locale + '.wikipedia.org/wiki/' + encodeURIComponent(title)); } ); }"
      },
      {
        app: apps.keys.Viki,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://(?:www\\\\.)?tamper\\\\.io/(?:curiosity|inquire)/share.*(?:(?:\\\\?|&)p=(\\\\d+).*&l=([a-zA-Z0-9\\\\-_]+)|(?:\\\\?|&)l=([a-zA-Z0-9\\\\-_]+).*&p=(\\\\d+)).*$'); var match = regex.exec(url); var locale = null; if (match[2] != null) { locale = match[2] } else if (match[3] != null) { locale = match[3]; } var pageid = null; if (match[1] != null) { pageid = match[1] } else if (match[4] != null) { pageid = match[4]; } url = 'https://' + locale + '.wikipedia.org/w/api.php?action=query&format=json&pageids=' + pageid; jsonRequest(url, function(res) { var key = Object.keys(res['query']['pages'])[0]; var title = res['query']['pages'][key]['title']; completionHandler('v-for-wiki://article/' + locale + '/' + encodeURIComponent(title)); } ); }"
      },
      {
        app: apps.keys.WikiLinks,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://(?:www\\\\.)?tamper\\\\.io/(?:curiosity|inquire)/share.*(?:(?:\\\\?|&)p=(\\\\d+).*&l=([a-zA-Z0-9\\\\-_]+)|(?:\\\\?|&)l=([a-zA-Z0-9\\\\-_]+).*&p=(\\\\d+)).*$'); var match = regex.exec(url); var locale = null; if (match[2] != null) { locale = match[2] } else if (match[3] != null) { locale = match[3]; } var pageid = null; if (match[1] != null) { pageid = match[1] } else if (match[4] != null) { pageid = match[4]; } url = 'https://' + locale + '.wikipedia.org/w/api.php?action=query&format=json&pageids=' + pageid; jsonRequest(url, function(res) { var key = Object.keys(res['query']['pages'])[0]; var title = res['query']['pages'][key]['title']; var outURL = 'https://' + locale + '.wikipedia.org/wiki/' + encodeURIComponent(title); completionHandler('wikilinks://openwikipediaurl?url=' + encodeURIComponent(btoa(outURL))); } ); }"
      },
      {
        app: apps.keys.Wikipanion,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://(?:www\\\\.)?tamper\\\\.io/(?:curiosity|inquire)/share.*(?:(?:\\\\?|&)p=(\\\\d+).*&l=([a-zA-Z0-9\\\\-_]+)|(?:\\\\?|&)l=([a-zA-Z0-9\\\\-_]+).*&p=(\\\\d+)).*$'); var match = regex.exec(url); var locale = null; if (match[2] != null) { locale = match[2] } else if (match[3] != null) { locale = match[3]; } var pageid = null; if (match[1] != null) { pageid = match[1] } else if (match[4] != null) { pageid = match[4]; } url = 'https://' + locale + '.wikipedia.org/w/api.php?action=query&format=json&pageids=' + pageid; jsonRequest(url, function(res) { var key = Object.keys(res['query']['pages'])[0]; var title = res['query']['pages'][key]['title']; completionHandler('wplink://' + locale + '.wikipedia.org/wiki/' + encodeURIComponent(title)); } ); }"
      },
      {
        "app": "wikipanionpad",
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://(?:www\\\\.)?tamper\\\\.io/(?:curiosity|inquire)/share.*(?:(?:\\\\?|&)p=(\\\\d+).*&l=([a-zA-Z0-9\\\\-_]+)|(?:\\\\?|&)l=([a-zA-Z0-9\\\\-_]+).*&p=(\\\\d+)).*$'); var match = regex.exec(url); var locale = null; if (match[2] != null) { locale = match[2] } else if (match[3] != null) { locale = match[3]; } var pageid = null; if (match[1] != null) { pageid = match[1] } else if (match[4] != null) { pageid = match[4]; } url = 'https://' + locale + '.wikipedia.org/w/api.php?action=query&format=json&pageids=' + pageid; jsonRequest(url, function(res) { var key = Object.keys(res['query']['pages'])[0]; var title = res['query']['pages'][key]['title']; completionHandler('wplink://' + locale + '.wikipedia.org/wiki/' + encodeURIComponent(title)); } ); }"
      },
      {
        app: apps.keys.Wikiwand,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://(?:www\\\\.)?tamper\\\\.io/(?:curiosity|inquire)/share.*(?:(?:\\\\?|&)p=(\\\\d+).*&l=([a-zA-Z0-9\\\\-_]+)|(?:\\\\?|&)l=([a-zA-Z0-9\\\\-_]+).*&p=(\\\\d+)).*$'); var match = regex.exec(url); var locale = null; if (match[2] != null) { locale = match[2] } else if (match[3] != null) { locale = match[3]; } var pageid = null; if (match[1] != null) { pageid = match[1] } else if (match[4] != null) { pageid = match[4]; } url = 'https://' + locale + '.wikipedia.org/w/api.php?action=query&format=json&pageids=' + pageid; jsonRequest(url, function(res) { var key = Object.keys(res['query']['pages'])[0]; var title = res['query']['pages'][key]['title']; completionHandler('wikiwand-article://' + locale + '/' + encodeURIComponent(title)); } ); }"
      },
      {
        app: apps.keys.Wonder,
        "script": "function process(url, completionHandler) { var regex = new RegExp('http(?:s)?://(?:www\\\\.)?tamper\\\\.io/(?:curiosity|inquire)/share.*(?:(?:\\\\?|&)p=(\\\\d+).*&l=([a-zA-Z0-9\\\\-_]+)|(?:\\\\?|&)l=([a-zA-Z0-9\\\\-_]+).*&p=(\\\\d+)).*$'); var match = regex.exec(url); var locale = null; if (match[2] != null) { locale = match[2] } else if (match[3] != null) { locale = match[3]; } var pageid = null; if (match[1] != null) { pageid = match[1] } else if (match[4] != null) { pageid = match[4]; } url = 'https://' + locale + '.wikipedia.org/w/api.php?action=query&format=json&pageids=' + pageid; jsonRequest(url, function(res) { var key = Object.keys(res['query']['pages'])[0]; var title = res['query']['pages'][key]['title']; completionHandler('wonder://' + locale + '.wikipedia.org/wiki/' + encodeURIComponent(title)); } ); }"
      },
      {
        app: apps.keys.Curiosity,
        "format": "curiosity://p/$2$3/$1$4"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s?)://((?:\\w+\\.)*github\\.com/.*)",
    "apps": [
      {
        app: apps.keys.iOctocat,
        "format": "ioc://$1"
      },
      {
        app: apps.keys.CodeHub,
        "format": "codehub://$1"
      }
    ]
  },
  {
    "title": "Show Repository",
    "regex": "http(?:s)?://(?:(?:(?!gist)\\w+\\.)*(?:(github)\\.com|(bitbucket)\\.org|(gitlab)\\.com)/([^/]+)/([^/]+?)(?:\\.git)?((/)?\\?.*$)?)$",
    "apps": [
      {
        app: apps.keys.WorkingCopy,
        "script": "function process(url, completionHandler) { completionHandler('working-copy://show?remote=' + encodeURIComponent(url)); }"
      },
      {
        app: apps.keys.Git2Go,
        "format": "gittogo://repositories/$1$2$3/$4/$5$6"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s?)://((?:\\w+\\.)*stackoverflow\\.com/.*)",
    "apps": [
      {
        app: apps.keys.StackExchange,
        "format": "com.stackexchange.stackoverflow://$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s?)://((?:\\w+\\.)*(?:(?:stackoverflow|stackexchange|serverfault|superuser|stackapps)\\.com|mathoverflow\\.net)/.*)",
    "apps": [
      {
        app: apps.keys.StackExchange,
        "format": "se-zaphod://$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://((?:\\w+\\.)+quora\\.com/.*)$",
    "apps": [
      {
        app: apps.keys.Quora,
        "format": "qhttp://$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:www\\.)zhihu\\.com/people/([a-zA-Z0-9_\\-%]+).*$",
    "apps": [
      {
        app: apps.keys.ZhiHu,
        "format": "zhihu://people/$1"
      }
    ]
  },
  {
    "title": "Open Topic",
    "regex": "http(?:s)?://(?:www\\.)zhihu\\.com/topic/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.ZhiHu,
        "format": "zhihu://topics/$1"
      }
    ]
  },
  {
    "title": "Open Question",
    "regex": "http(?:s)?://(?:www\\.)zhihu\\.com/question/(\\d+)(?!.*/answer/\\d+).*$",
    "apps": [
      {
        app: apps.keys.ZhiHu,
        "format": "zhihu://questions/$1"
      }
    ]
  },
  {
    "title": "Open Answer",
    "regex": "http(?:s)?://(?:www\\.)zhihu\\.com/question/(?:\\d+)/answer/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.ZhiHu,
        "format": "zhihu://answers/$1"
      }
    ]
  },
  {
    "title": "Open Note",
    "regex": "http(?:s)?://(?:www\\.)evernote\\.com/shard/s\\d+/sh/([:hex:]+-)*[:hex:]+/[:hex:]+.*$",
    "apps": [
      {
        app: apps.keys.Evernote,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(evernote://.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Document",
    "regex": "http(?:s)?://(?:\\w+\\.)*quip\\.com/([a-zA-Z0-9]{12}).*$",
    "apps": [
      {
        app: apps.keys.Quip,
        "format": "quip://$1"
      }
    ]
  },
  {
    "title": "Open Board",
    "regex": "http(?:s)?://(?:www\\.)?trello\\.com/b/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Trello,
        "format": "trello://x-callback-url/showBoard?shortlink=$1"
      }
    ]
  },
  {
    "title": "Open Card",
    "regex": "http(?:s)?://(?:www\\.)?trello\\.com/c/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Trello,
        "format": "trello://x-callback-url/showCard?shortlink=$1"
      }
    ]
  },
  {
    "title": "Open Action",
    "regex": "http(?:s)?://(?:www\\.)?launchcenterpro\\.com/(\\w{6}).*$",
    "apps": [
      {
        app: apps.keys.LaunchCenterPro,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(launch://import.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      },
      {
        app: apps.keys.LaunchCenterProIpad,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(launch://import.*?)\"'); var match = regex.exec(res)[1]; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Recipe",
    "regex": "http(?:s?)://(?:www\\.)?ifttt\\.com/recipes/([0-9]+).*$",
    "apps": [
      {
        app: apps.keys.IFTTT,
        "format": "ifttt://shared_recipe/$1"
      }
    ]
  },
  {
    "title": "Add Workflow",
    "regex": "http(?:s?)://(?:www\\.)?workflow.is/workflows/([:hex:]+).*$",
    "apps": [
      {
        app: apps.keys.Workflow,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(workflow://workflows/.*)\"'); var match = regex.exec(res)[1]; match = htmlDecode(match); completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?houzz(?:\\.\\w+)+/((?:photos|ideabooks)/(\\d+)).*$",
    "apps": [
      {
        app: apps.keys.Houzz,
        "format": "houzz:///$1"
      }
    ]
  },
  {
    "title": "Open Details",
    "regex": "http(?:s)?://(?:\\w+\\.)?zillow\\.com/homedetails/.*?(\\d+)_zpid.*$",
    "apps": [
      {
        app: apps.keys.Zillow,
        "format": "zillowapp://hdp/$1"
      }
    ]
  },
  {
    "title": "Open Listing",
    "regex": "http(?:s)?://(?:www\\.)?redfin\\.com/[a-zA-Z]{2}/(?:[a-zA-Z0-9_\\-]+/){0,2}home/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.REDFIN,
        "format": "redfin://home/$1"
      }
    ]
  },
  {
    "title": "Open Listing",
    "regex": "http(?:s)?://(?:\\w+\\.)?trulia\\.com/property/\\d+.*$",
    "apps": [
      {
        app: apps.keys.Trulia,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(urlHash=.*?)\"'); var match = regex.exec(res)[1]; completionHandler('truliapdpforsale://?' + match); }"
      }
    ]
  },
  {
    "title": "Open Listing",
    "regex": "http(?:s)?://(?:\\w+\\.)?trulia\\.com/rental(?:-community)?/\\d+.*$",
    "apps": [
      {
        app: apps.keys.TruliaRentals,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(urlHash=.*?)\"'); var match = regex.exec(res)[1]; completionHandler('truliapdpforrent://?' + match); }"
      }
    ]
  },
  {
    "title": "Open Illustration",
    "regex": "http(?:s)?://(?:\\w+\\.)?pixiv\\.net/member_illust\\.php?.*illust_id=(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Pixiv,
        "format": "pixiv://illusts/$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:\\w+\\.)?pixiv\\.net/member\\.php?.*id=(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Pixiv,
        "format": "pixiv://users/$1"
      }
    ]
  },
  {
    "title": "View App",
    "regex": "http(?:s)?://(?:geo\\.)?itunes\\.apple\\.com/(?:.*/)*app/(?:.*/)?id(\\d+)(?!.*(?:&|\\?)mt=12.*).*$",
    "apps": [
      {
        app: apps.keys.AppStore,
        "format": "opener://x-callback-url/show-store-product-details?id=$1"
      }
    ]
  },
  {
    "title": "View App",
    "regex": "http(?:s)?://(?:geo\\.)?itunes\\.apple\\.com/(?:.*/)*app/(?:.*/)?id(\\d+).*$",
    "apps": [
      {
        app: apps.keys.AppShopper,
        "format": "appshoppersocial://app/$1"
      },
      {
        app: apps.keys.AppZapp,
        "format": "appzapp://app?appid=$1"
      },
      {
        app: apps.keys.CatchApp,
        "format": "catchapp://app?id=$1"
      },
      {
        app: apps.keys.PriceTag,
        "format": "pricetag://activity?id=$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:geo\\.)?itunes\\.apple\\.com/(?:.*/)*(artist|album|music-video)/(?:.*/)?id(\\d+).*?(?:(?:\\?|&)(i=(?:\\d+)))?.*?$",
    "apps": [
      {
        app: apps.keys.iTunesStore,
        "format": "itms://itunes.apple.com/$1/id$2?at=1001l3kM&$3"
      },
      {
        app: apps.keys.AppleMusic,
        "format": "music://itunes.apple.com/$1/id$2?at=1001l3kM&$3"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:geo\\.)?itunes\\.apple\\.com/(?:.*/)*(movie|tv-season)/(?:.*/)?id(\\d+).*?(?:(?:\\?|&)(i=(?:\\d+)))?.*?$",
    "apps": [
      {
        app: apps.keys.iTunesStore,
        "format": "itms://itunes.apple.com/$1/id$2?at=1001l3kM&$3"
      }
    ]
  },
  {
    "title": "Open Playlist",
    "regex": "http(?:s)?://((?:geo\\.)?itunes\\.apple\\.com/(?:.*/)?playlist/.*)$",
    "apps": [
      {
        app: apps.keys.AppleMusic,
        "format": "music://$1"
      }
    ]
  },
  {
    "title": "Convert Link",
    "regex": "(http(?:s)?://(?:geo\\.)?itunes\\.apple\\.com/(?!.*/podcast/.*)(?:.*/)?id(\\d+).*)$",
    "apps": [
      {
        "app": "blink",
        "format": "blink://x-callback-url/search?q=$1"
      }
    ]
  },
  {
    "title": "Open Deal",
    "regex": "http(?:s)?://(?:www\\.)?slickdeals\\.net/f/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Slickdeals,
        "format": "slickdeals://thread/$1"
      }
    ]
  },
  {
    "title": "Open Story",
    "regex": "http(?:s)?://abcnews\\.go\\.com/.*(?:(?:\\w+-)|/story.*(?:\\?|&)id=)+(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.ABCNews,
        "format": "abcnewsiphone://link/story,$1"
      },
      {
        app: apps.keys.ABCNewsIpad,
        "format": "abcnewsipad://link/story,$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)?imgur\\.com/(?:gallery/|a/|r/.*/)?(\\w+).*?$",
    "apps": [
      {
        app: apps.keys.Imgur,
        "format": "imgur://imgur.com/$1"
      }
    ]
  },
  {
    "title": "Open Shot",
    "regex": "http(?:s)?://(?:(?!help)\\w+\\.)?dribbble\\.com/shots/(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Zeeen,
        "format": "zeeen://shot/$1"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://(?:(?!help)\\w+\\.)?dribbble\\.com/(?:player(?:s)?/)?(?!shots|session|signup|search|skills|teams|meetups|stories|jobs|about)(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Zeeen,
        "format": "zeeen://player/$1"
      }
    ]
  },
  {
    "title": "Open Issue",
    "regex": "http(?:s)?://((\\w+\\.)*jira(\\.\\w+)+.*$)",
    "apps": [
      {
        app: apps.keys.JIRA,
        "format": "jira://$1"
      },
      {
        app: apps.keys.JIRAPad,
        "format": "jira://$1"
      }
    ]
  },
  {
    "title": "Open Track",
    "regex": "http(?:s)?://(?:www\\.)?tidal\\.com/(?:#!/)?track/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.TIDAL,
        "format": "tidal://track/$1"
      }
    ]
  },
  {
    "title": "Open Album",
    "regex": "http(?:s)?://(?:www\\.)?tidal\\.com/(?:#!/)?album/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.TIDAL,
        "format": "tidal://album/$1"
      }
    ]
  },
  {
    "title": "Open Artist",
    "regex": "http(?:s)?://(?:www\\.)?tidal\\.com/(?:#!/)?artist/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.TIDAL,
        "format": "tidal://artist/$1"
      }
    ]
  },
  {
    "title": "Open Playlist",
    "regex": "http(?:s)?://(?:www\\.)?tidal\\.com/(?:#!/)?playlist/([a-fA-F0-9\\-]+).*$",
    "apps": [
      {
        app: apps.keys.TIDAL,
        "format": "tidal://playlist/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://((?:\\w+\\.)*(?:taobao|tmall)\\.com/.*)$",
    "apps": [
      {
        app: apps.keys.Taobao,
        "format": "taobao://$1"
      },
      {
        app: apps.keys.TaobaoPad,
        "format": "taobao://$1"
      }
    ]
  },
  {
    "title": "Open Item",
    "regex": "http(?:s)?://(?:(?:\\w+\\.)*jd\\.(?:com|hk)/(?:.*/)?(\\d+)\\.(?:s)?html|(?:\\w+\\.)*jd\\.(?:com|hk)/(?:ware/view\\.action|my/).*(?:wareId|sku)=(\\d+)).*?$",
    "apps": [
      {
        app: apps.keys.JingDong,
        "format": "openapp.jdmobile://virtual?params=%7B%22category%22%3A%22jump%22%2C%22des%22%3A%22productDetail%22%2C%22sourceType%22%3A%22unknown%22%2C%22sourceValue%22%3A%22unknown%22%2C%22skuId%22%3A%22$1$2%22%7D"
      },
      {
        app: apps.keys.JingDongPad,
        "format": "openapp.jdiPad://virtual?params=%7B%22category%22%3A%22jump%22%2C%22des%22%3A%22productDetail%22%2C%22sourceType%22%3A%22unknown%22%2C%22sourceValue%22%3A%22unknown%22%2C%22skuId%22%3A%22$1$2%22%7D"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://tieba\\.baidu\\.(?:com|cn)/p/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.BaidoTieba,
        "format": "com.baidu.tieba://jumptoforum?kz=$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?vk\\.(?:com|ru)/(.*)$",
    "apps": [
      {
        app: apps.keys.VK,
        "format": "vk://vk.com/$1"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?(?:tools\\.|trkcnfrm\\.smi\\.|trkcnfrm1\\.smi\\.)?usps\\.com.*(?:\\?|&)(?:tLabels|strOrigTrackNum|origTrackNum|qtc_tLabels1|formattedLabel)=(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?(?:wwwapps\\.|campusship\\.)?ups\\.com.*(?:\\?|&)(?:InquiryNumber1|tracknum|trackNums|inquiry1|trackingNumber|InquiryNumber)=(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?(?:spportal\\.)?(?:fedex|federalexpress)\\.com.*(?:\\?|&)(?:tracknumbers|tracknums|trackNum|track_number_0|tracknumber_list|TRACKING|PID)=(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?(?:track\\.)?(?:dhl|dhl-usa|airborne)\\.com.*(?:\\?|&)(?:AWB|ShipmentNumber|shipmentNumber)=(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?(?:webtrack\\.)?(?:dhlglobalmail\\.com|dhlgm\\.mytracking\\.net).*(?:\\?|&)(?:trackingnumber|TrackingID)=(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:(?:\\w+\\.)*dhl\\.de|webportal-at\\.dhl\\.com).*(?:\\?|&)(?:idc|packet_id|sid|AWB|ic)=(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:\\w+\\.)*dhl\\.co\\.uk.*(?:\\?|&)PCL_NO=(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://checkout\\.google\\.com.*(?:\\?|&)t=(\\d+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:\\w+\\.)?ontrac\\.com.*(?:\\?|&)tracking_number=(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:www\\.)?(?:secure\\.|secure1\\.|secure2\\.)?(?:store\\.)?apple\\.com/(?:.*)?(?!hk-zh/|jp/)(?:order/guest/|vieworder/|orderNumberField=|olsson=)(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:www\\.)?(?:secure\\.|secure1\\.|secure2\\.)?(?:store\\.)?apple\\.com/(?:.*)?hk-zh(/.*)?(?:order/guest/|vieworder/|orderNumberField=|olsson=)(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:www\\.)?(?:secure\\.|secure1\\.|secure2\\.)?(?:store\\.)?apple\\.com/(?:.*)?hk-zh(/.*)?(?:order/guest/|vieworder/|orderNumberField=|olsson=)(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:www\\.)?applestore\\.bridge-point\\.com.*(?:\\?|&)sc=(\\w+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Add Tracking Info",
    "regex": "http(?:s)?://(?:\\w+\\.)*(amazon(?:\\.\\w+)+).*(?:\\?|&)(?:trackingNumber|orderId|orderID|oid|orderIDs)=(\\d{3,3}-\\d+-\\d+).*$",
    "apps": [
      {
        app: apps.keys.Deliveries,
        "script": "function process(url, completionHandler) { completionHandler('deliveries://add/' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Open Story",
    "regex": "http(?:s)?://(?:www\\.)?apple\\.news/([a-zA-Z0-9_\\-]+).*?$",
    "apps": [
      {
        app: apps.keys.AppleNews,
        "format": "applenews:/$1"
      }
    ]
  },
  {
    "title": "Open Restaurant",
    "regex": "http(?:s)?://(?:\\w+\\.)+?opentable(?:\\.\\w+)+(?:/restaurants/.*/(\\d+)|.*rid=(\\d+).*||.*\\n.*er=(\\d+).*)$",
    "apps": [
      {
        app: apps.keys.OpenTable,
        "format": "reservetable-com.contextoptional.OpenTable-1://?rid=$1$2$3"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:\\w+\\.)?(?:nicovideo\\.jp/watch/|nico\\.ms/)((?:sm)?\\d+).*$",
    "apps": [
      {
        app: apps.keys.Niconico,
        "format": "niconico://$1"
      },
      {
        app: apps.keys.iNico2,
        "format": "inico2http://www.nicovideo.jp/watch/$1"
      },
      {
        app: apps.keys.SmilePlayer2,
        "format": "smileplayer2://id/$1"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:\\w+\\.)?(?:nicovideo\\.jp/watch/|nico\\.ms/)(so\\d+).*$",
    "apps": [
      {
        app: apps.keys.iNico2,
        "format": "inico2http://www.nicovideo.jp/watch/$1"
      },
      {
        app: apps.keys.SmilePlayer2,
        "format": "smileplayer2://id/$1"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:\\w+\\.)?(?:nicovideo\\.jp/watch/|nico\\.ms/)(nm\\d+).*$",
    "apps": [
      {
        app: apps.keys.Niconico,
        "format": "niconico://$1"
      },
      {
        app: apps.keys.iNico2,
        "format": "inico2http://www.nicovideo.jp/watch/$1"
      }
    ]
  },
  {
    "title": "Open Live Video",
    "regex": "http(?:s)?://(?:\\w+\\.)?(?:nicovideo\\.jp/watch/|nico\\.ms/)(lv\\d+).*$",
    "apps": [
      {
        app: apps.keys.Niconico,
        "format": "niconico://$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?(wolframalpha\\.com/input.*)$",
    "apps": [
      {
        app: apps.keys.WolframAlpha,
        "format": "wolframalpha://$1"
      }
    ]
  },
  {
    "title": "Open Story",
    "regex": "http(?:s)?://(?:www\\.|m\\.|mobile\\.)(nytimes\\.com/\\d+/\\d+/\\d+.*)$",
    "apps": [
      {
        app: apps.keys.NewYorkTimes,
        "format": "nytimes://www.$1"
      }
    ]
  },
  {
    "title": "Open Task",
    "regex": "http(?:s)?://(?:\\w+\\.)*asana\\.com/(\\d/\\d+/\\d+.*)$",
    "apps": [
      {
        app: apps.keys.Asana,
        "format": "asana://app.asana.com/$1"
      }
    ]
  },
  {
    "title": "Import PDF",
    "regex": "(http(?:s)?://([^?]+(?:\\.pdf).*))$",
    "apps": [
      {
        app: apps.keys.PDFExpert,
        "format": "pdfehttp://$2"
      },
      {
        app: apps.keys.PDFViewer,
        "format": "pdfviewer://x-callback-url/add-file?url=$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)?goodreads\\.com/(.*)$",
    "apps": [
      {
        app: apps.keys.Goodreads,
        "format": "goodreads://$1"
      }
    ]
  },
  {
    "title": "Open Doc",
    "regex": "http(?:s)?://(docs\\.google\\.com/.*document/d/.*)$",
    "apps": [
      {
        app: apps.keys.GoogleDocs,
        "format": "googledocs://$1"
      }
    ]
  },
  {
    "title": "Open Sheet",
    "regex": "http(?:s)?://(docs\\.google\\.com/.*spreadsheets/d/.*)$",
    "apps": [
      {
        app: apps.keys.GoogleSheets,
        "format": "googlesheets://$1"
      }
    ]
  },
  {
    "title": "Open Slides",
    "regex": "http(?:s)?://(docs\\.google\\.com/.*presentation/d/.*)$",
    "apps": [
      {
        app: apps.keys.GoogleSlides,
        "format": "googleslides://$1"
      }
    ]
  },
  {
    "title": "Add User",
    "regex": "http(?:s)?://(?:www\\.)?snapchat\\.com/add/([^/]+).*?$",
    "apps": [
      {
        app: apps.keys.Snapchat,
        "format": "snapchat://add/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?snapchat\\.com/unlock(.*)$",
    "apps": [
      {
        app: apps.keys.Snapchat,
        "format": "snapchat://unlock$1"
      }
    ]
  },
  {
    "title": "Add User",
    "regex": "http(?:s)?://(?:www\\.)?peach\\.cool/add/([^/]+).*?$",
    "apps": [
      {
        app: apps.keys.Peach,
        "format": "peach://add/$1"
      }
    ]
  },
  {
    "title": "Open Post",
    "regex": "http(?:s)?://(?:www\\.)?wear\\.jp/[^/]+/coordinate/(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.WEAR,
        "format": "wear2020://wear.jp/coordinate/$1"
      }
    ]
  },
  {
    "title": "Open Story",
    "regex": "http(?:s)?://(?:\\w+\\.)*media\\.daum\\.net/(?:m/media/(?:[^/]+/)?newsview/|v/)(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Daum,
        "format": "daumapps://web?url=http%3A%2F%2Fv.media.daum.net%2Fv%2F$1%3Ff%3Dm"
      }
    ]
  },
  {
    "title": "Open Profile",
    "regex": "http(?:s)?://([^\\.]+)\\.deviantart\\.com(?:(/)?\\?.*|/(gallery(/)?(\\?.*)?)?)?$",
    "apps": [
      {
        app: apps.keys.DeviantArt,
        "format": "deviantart://profile/$1"
      }
    ]
  },
  {
    "title": "Open Gallery",
    "regex": "http(?:s)?://([^\\.]+)\\.deviantart\\.com/gallery/\\d+.*$",
    "apps": [
      {
        app: apps.keys.DeviantArt,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('://gallery/([^\\\"]+)'); var match = regex.exec(res)[1]; completionHandler('deviantart://gallery/' + match); }"
      }
    ]
  },
  {
    "title": "Open Collection",
    "regex": "http(?:s)?://([^\\.]+)\\.deviantart\\.com/favourites/\\d+.*$",
    "apps": [
      {
        app: apps.keys.DeviantArt,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('://collection/([^\\\"]+)'); var match = regex.exec(res)[1]; completionHandler('deviantart://collection/' + match); }"
      }
    ]
  },
  {
    "title": "Open Deviation",
    "regex": "http(?:s)?://(([^\\.]+)\\.deviantart\\.com/(art|journal)/|sta\\.sh/).*$",
    "apps": [
      {
        app: apps.keys.DeviantArt,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('://deviation/([^\\\"]+)'); var match = regex.exec(res)[1]; completionHandler('deviantart://deviation/' + match); }"
      }
    ]
  },
  {
    "title": "Open Tag",
    "regex": "http(?:s)?://(?:\\w+\\.)*deviantart\\.com/tag/([^/]+).*$",
    "apps": [
      {
        app: apps.keys.DeviantArt,
        "format": "deviantart://tag/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?anchor\\.fm/(w/.*)$",
    "apps": [
      {
        app: apps.keys.Ancher,
        "format": "anchorfm://app/$1"
      }
    ]
  },
  {
    "title": "Open Story",
    "regex": "http(?:s)?://(?:\\w+\\.)*9gag\\.com/gag/([^/]+).*$",
    "apps": [
      {
        app: apps.keys['9GAG'],
        "format": "ninegag://9gag.com/gag/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)?tabelog\\.com/(?:\\w+/){3,4}(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Tabelog,
        "format": "tabelog-v2://rstdtl/$1"
      }
    ]
  },
  {
    "title": "Open Auction",
    "regex": "http(?:s)?://(?:\\w+\\.)*auctions\\.yahoo\\.co\\.jp(?:/.*)?/auction/(\\w+).*$",
    "apps": [
      {
        app: apps.keys.YahooAuctionsJP,
        "format": "yjauctions://auctionitem?auctionid=$1"
      }
    ]
  },
  {
    "title": "Open ",
    "regex": "http(?:s)?://(?:www\\.)?ticketfly\\.com/(?:purchase/)?event/(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Ticketfly,
        "format": "ticketfly://app/events/$1"
      }
    ]
  },
  {
    "title": "Open Recommendation",
    "regex": "http(?:s)?://rex\\.is/p/(\\d+).*?$",
    "apps": [
      {
        "app": "rex",
        "format": "rex://pick/$1/view"
      }
    ]
  },
  {
    "title": "Open Song",
    "regex": "http(?:s)?://(?:www\\.)?(soundhound\\.com/?.*t=\\d+.*)$",
    "apps": [
      {
        app: apps.keys.SoundHound,
        "format": "soundhound://$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:[^\\.])+\\.hatenablog(?:\\.\\w+)+.*$",
    "apps": [
      {
        app: apps.keys.HatenaBlog,
        "script": "function process(url, completionHandler) { completionHandler('hatenablog:///open?uri=' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Open Story",
    "regex": "http(?:s)?://(?:\\w+\\.)*thescore\\.com/(?:[^/]+/)?news/(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.theScore,
        "format": "thescore:///news/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)*glassdoor\\.com/(.*)$",
    "apps": [
      {
        app: apps.keys.Glassdoor,
        "format": "glassdoor://$1"
      }
    ]
  },
  {
    "title": "Open Chat",
    "regex": "http(?:s)://(?:www\\.)?(?:telegram|t)\\.me/(?!addstickers)([^/\\?#]+).*$",
    "apps": [
      {
        app: apps.keys.Telegram,
        "format": "tg://resolve?domain=$1"
      }
    ]
  },
  {
    "title": "Add Stickers",
    "regex": "http(?:s)://(?:www\\.)?(?:telegram|t)\\.me/addstickers/([^/\\?#]+).*$",
    "apps": [
      {
        app: apps.keys.Telegram,
        "format": "tg://addstickers?set=$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)*mobile01\\.com/(\\w+detail)\\.php\\?(.*)$",
    "apps": [
      {
        app: apps.keys.Mobile01,
        "format": "m01://$1?$2"
      }
    ]
  },
  {
    "title": "View Lyrics",
    "regex": "http(?:s)?://(?:www\\.)?genius\\.com/(\\d+|.*-lyrics).*$",
    "apps": [
      {
        app: apps.keys.Genius,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(genius://.*?)\"'); var results = regex.exec(res); var match = null; if (results != null && results.length > 1) { match = results[1] }; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)*espn\\.com(?:\\.\\w+)?/(?!video).*id/(\\d+).*$",
    "apps": [
      {
        app: apps.keys.ESPN,
        "format": "sportscenter://x-callback-url/showStory?uid=$1"
      }
    ]
  },
  {
    "title": "Open Media",
    "regex": "(http(?:s)?://([^:/\\s]+)/[^?]+\\.(mp4|mkv|tp|mov|avi|wmv|asf|flv|ogv|rmvb|mp3|wav|wma|flac|ape))/?(\\?.*)?$",
    "apps": [
      {
        app: apps.keys.nPlayer,
        "format": "nplayer-$1"
      }
    ]
  },
  {
    "title": "Stream Media",
    "regex": "(http(?:s)?://([^:/\\s]+)/[^?]+\\.(mpeg|mp1v|mpg1|PIM1|mp2v|mpg2|vcr2|hdv1|hdv2|hdv3|mx.n|mx.p|DIV1|DIV2|DIV3|mp4|mp41|mp42|MPG4|MPG3|DIV4|DIV5|DIV6|col1|col0|3ivd|DIVX|Xvid|mp4s|m4s2|xvid|mp4v|fmp4|3iv2|smp4|h261|h262|h263|h264|s264|AVC1|DAVC|H264|X264|VSSH|SVQ.|cvid|thra|wmv1|wmv2|wmv3|wvc1|wmva|VP31|VP30|VP3|VP50|VP5|VP51|VP60|VP61|VP62|VP6F|VP6A|VP7|FSV1|IV31|IV32|IV41|IV51|RV10|RV13|RV20|RV30|RV40|BBCD|wmv|mpga|mp3|LAME|mp4a|a52|a52b|atrc|ILBC|Qclp|lpcJ|28_8|dnet|sipr|cook|atrc|raac|racp|ralf|shrn|spex|vorb|ogg|dts|wma|wma1|wma2|flac|alac|samr|SONC|3gp|asf|au|avi|flv|mov|ogm|mkv|mka|ts|mpg|mp2|nsc|nsv|nut|ra|ram|rm|tv|rmbv|a52|dts|aac|dv|vid|tta|tac|ty|wav|dts|xa))/?(\\?.*)?$",
    "apps": [
      {
        app: apps.keys.VLC,
        "script": "function process(url, completionHandler) { completionHandler('vlc-x-callback://x-callback-url/stream?url=' + encodeURIComponent(url)); }"
      },
      {
        app: apps.keys.Infuse,
        "script": "function process(url, completionHandler) { completionHandler('infuse://x-callback-url/play?url=' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Download Media",
    "regex": "(http(?:s)?://([^:/\\s]+)/[^?]+\\.(mpeg|mp1v|mpg1|PIM1|mp2v|mpg2|vcr2|hdv1|hdv2|hdv3|mx.n|mx.p|DIV1|DIV2|DIV3|mp4|mp41|mp42|MPG4|MPG3|DIV4|DIV5|DIV6|col1|col0|3ivd|DIVX|Xvid|mp4s|m4s2|xvid|mp4v|fmp4|3iv2|smp4|h261|h262|h263|h264|s264|AVC1|DAVC|H264|X264|VSSH|SVQ.|cvid|thra|wmv1|wmv2|wmv3|wvc1|wmva|VP31|VP30|VP3|VP50|VP5|VP51|VP60|VP61|VP62|VP6F|VP6A|VP7|FSV1|IV31|IV32|IV41|IV51|RV10|RV13|RV20|RV30|RV40|BBCD|wmv|mpga|mp3|LAME|mp4a|a52|a52b|atrc|ILBC|Qclp|lpcJ|28_8|dnet|sipr|cook|atrc|raac|racp|ralf|shrn|spex|vorb|ogg|dts|wma|wma1|wma2|flac|alac|samr|SONC|3gp|asf|au|avi|flv|mov|ogm|mkv|mka|ts|mpg|mp2|nsc|nsv|nut|ra|ram|rm|tv|rmbv|a52|dts|aac|dv|vid|tta|tac|ty|wav|dts|xa))/?(\\?.*)?$",
    "apps": [
      {
        app: apps.keys.VLC,
        "script": "function process(url, completionHandler) { completionHandler('vlc-x-callback://x-callback-url/download?url=' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Open Note",
    "regex": "http(s)?://(www\\.)?jianshu\\.com/p/\\w+",
    "apps": [
      {
        app: apps.keys.Jianshu,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(jianshu://notes/\\\\d+)'); var results = regex.exec(res); var match = null; if (results != null && results.length > 1) { match = results[1] }; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Geocache",
    "regex": "http(?:s)?://(?:www\\.)?(?:geocaching\\.com/geocache|coord\\.info)/(GC[A-Za-z0-9]+).*$",
    "apps": [
      {
        app: apps.keys.Cachly,
        "format": "cachly://gccode=$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?netflix\\.com/((watch|title)/.*)$",
    "apps": [
      {
        app: apps.keys.Netflix,
        "format": "nflx://$1"
      }
    ]
  },
  {
    "title": "Open Sale",
    "regex": "http(?:s)?://(?:\\w+\\.)?wallapop\\.com/(?:i/|item/.*-)(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Wallapop,
        "format": "wallapop://i/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.|m\\.)?douyu\\.(?:com|tv)/(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.Douyu,
        "format": "douyutv://$1&0&"
      }
    ]
  },
  {
    "title": "Open Video",
    "regex": "http(?:s)?://(?:www\\.)?abemafresh\\.tv/(?:[^/]+)/(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.FRESHLIVE,
        "format": "amebafresh://program/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "(http(?:s)?://(\\w+\\.)?namu\\.wiki/.*)$",
    "apps": [
      {
        app: apps.keys.NamuViewer,
        "format": "namuviewer://?url=$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)?pandora\\.com/(?!station/play/).*",
    "apps": [
      {
        app: apps.keys.Pandora,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(pandorav\\\\d+:/.*?)\"'); var results = regex.exec(res); var match = null; if (results != null && results.length > 1) { match = results[1] }; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)?pandora\\.com/station/play/(\\d+).*?",
    "apps": [
      {
        app: apps.keys.Pandora,
        "format": "pandorav2:/createStation?stationId=$1"
      }
    ]
  },
  {
    "title": "Open Article",
    "regex": "http(?:s)?://(?:\\w+\\.)?wsj\\.com/articles/.*$",
    "apps": [
      {
        app: apps.keys.TheWallStreetJournal,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('(wsj://.*?)\"'); var results = regex.exec(res); var match = null; if (results != null && results.length > 1) { match = results[1].replace(new RegExp('&headline=[^&]*'), '') + '&headline=' }; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)?mixcloud\\.com/(.+)$",
    "apps": [
      {
        app: apps.keys.Mxcloud,
        "format": "mc://$1"
      }
    ]
  },
  {
    "title": "Open Story",
    "regex": "http(?:s)?://(?:\\w+\\.)?lemonde\\.fr/[^/]+/article/.*?_(\\d+).*?$",
    "apps": [
      {
        app: apps.keys.LeMonde,
        "format": "lmfr://element/article/$1"
      }
    ]
  },
  {
    "title": "Open Slides",
    "regex": "http(s)?://(www\\.)?slideshare\\.net/[^/]+/[^/|\\?]+.*$",
    "apps": [
      {
        app: apps.keys.SlideShare,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('.*(slideshare-app://ss/\\\\d+).*'); var results = regex.exec(res); var match = null; if (results != null && results.length > 1) { match = results[1] }; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:www\\.)?(appadvice\\.com/.*)$",
    "apps": [
      {
        app: apps.keys.AppAdvice,
        "format": "aaexternal://$1"
      }
    ]
  },
  {
    "title": "Open Doc",
    "regex": "http(?:s)?://paper\\.dropbox\\.com/doc/.*$",
    "apps": [
      {
        app: apps.keys.DropboxPaper,
        "script": "function process(url, completionHandler) { completionHandler('dbx-paper://open-doc?url=' + encodeURIComponent(url)); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://music\\.163\\.com/(?:(?:m|#)/)?(song|album|playlist|program)(?:(?:.*(?:&|\\?)id=|/))(\\d+).*$",
    "apps": [
      {
        app: apps.keys['163Music'],
        "format": "orpheus://$1/$2"
      }
    ]
  },
  {
    "title": "Open Story",
    "regex": "http(?:s)?://(?:www\\.)?blendle\\.com/(?:i|item)/(?:[^/]+/[^/]+/)?([^/|\\?]*).*$",
    "apps": [
      {
        app: apps.keys.Blendle,
        "format": "blendle://item/$1"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)?(?:zomato\\.com|zoma\\.to).*$",
    "apps": [
      {
        app: apps.keys.Zomato,
        "script": "function process(url, completionHandler) { var res = $http.sync(url); var regex = RegExp('.*(zomato://[^\"]+).*'); var results = regex.exec(res); var match = null; if (results != null && results.length > 1) { match = results[1] }; completionHandler(match); }"
      }
    ]
  },
  {
    "title": "Open Link",
    "regex": "http(?:s)?://(?:\\w+\\.)?eventshigh\\.com/(.*)$",
    "apps": [
      {
        app: apps.keys.EventsHigh,
        "format": "ehapp://$1"
      }
    ]
  }
]
