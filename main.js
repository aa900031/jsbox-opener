const opener = require('./scripts/opener');

const getSchemeMap = async (link) => {
  const { rule, matches } = opener.match(link)
  if (!rule || !matches) return

  const schemes = await Promise.all(
    rule.apps.map(app => opener.scheme(app, link, matches)),
  )

  return schemes.reduce((map, scheme, index) => {
    const app = rule.apps[index]
    if (app && app.app) {
      map[app.app] = scheme
    }

    return map
  }, {})
}

const getLink = () => {
   switch ($app.env) {
     case $env.action:
       return $context.link;
     case $env.safari:
       return $context.safari.items.baseURI;
     case $env.siri:
       return $context.query.url;
     default:
       return;
   }
}

const getHasJsBoxUI = () => {
  switch ($app.env) {
    case $env.siri:
      return false
    default:
      return true
  }
}

(async () => {
  const isNeedUI = getHasJsBoxUI()
  const link = getLink()
  if (!link) {
    isNeedUI && $ui.alert('沒有傳入連結')
    return
  }

  const schemes = await getSchemeMap(link)
  if (!schemes) {
    isNeedUI && $ui.alert('沒有在規則內的 URL')
    return
  }

  const keys = Object.keys(schemes)

  if (isNeedUI) {
    const result = await $ui.menu({
      items: keys.map((key) => $l10n(key)),
    })

    const scheme = schemes[keys[result.index]]
    $app.openURL(scheme)
  } else {
    $intents.finish(keys.reduce((map, key) => {
      map[$l10n(key)] = schemes[key]
      return map
    }, {}))
  }
})()
